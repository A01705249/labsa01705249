<?php
    session_start();

    // Include cleanup utilities and users model with absolute path
    $cleanup_utils_absolute_path = $_SERVER['DOCUMENT_ROOT']."/labsa01705249/lab18/utils/cleanup.php";
    $user_model_absolute_path = $_SERVER['DOCUMENT_ROOT']."/labsa01705249/lab18/models/users_model.php";
    require_once($cleanup_utils_absolute_path);
    require_once($user_model_absolute_path);

    // If the form has been submitted
    if (isset($_POST["submit"])) {
        if(isset($_POST["username"]) && isset($_POST["password"])) {

            // Sanitize input data
            $username = cleanup_input($_POST["username"]);
            $password = cleanup_input($_POST["password"]);

            if(user_exists($username)) {
                if(valid_credentials($username, $password)) {
                    $_SESSION["username"] = $username;
                    header("Location: ../schedule.php");
                }
                else {
                    $_SESSION["password_error"] = "La contraseña es incorrecta";
                    $_SESSION["username_value"] = $_POST["username"];
                    header("Location: ../login.php");
                }
            }   
            else {
                if($username !== "") {
                    register_user($username, $password);
                    $_SESSION["username"] = $username;
                    header("Location: ../schedule.php");
                } 
                else {
                    $_SESSION["username_error"] = "Por favor ingresa un nombre de usuario";
                    header("Location: ../login.php");
                }
            }
        } 
        else {
            $_SESSION["form_error"] = "El formulario esta incompleto";
            header("Location: ../login.php");
        }
    }
    else {
        header("Location: ../login.php");
    }

?>