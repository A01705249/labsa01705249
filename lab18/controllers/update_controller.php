<?php
    session_start();

    // Include cleanup utilities and users model with absolute path
    $schedule_utils_absolute_path = $_SERVER['DOCUMENT_ROOT']."/labsa01705249/lab18/utils/schedule_utils.php";
    $schedule_model_absolute_path = $_SERVER['DOCUMENT_ROOT']."/labsa01705249/lab18/models/schedule_model.php";
    require_once($schedule_utils_absolute_path);
    require_once($schedule_model_absolute_path);

    // Updates the database to reflect the user's selection
    update_user_schedule($_POST["selected_cells"], $_SESSION["username"]);

    // The text of this controller. Will contain the group 
    // schedule including the changes just made by the user
    echo get_group_schedule_html();   
?>