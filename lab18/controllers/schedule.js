initialize();

function initialize() {
    attach_cell_listeners();

    // The selection ends whenever the user lifts the mouse button
    // in the ENTIRE window as opossed to just the schedule area
    // because it's possible that the users drags the the cursor
    // outside of said area
    window.onmouseup = selection_end; 
}

// Assigs functions to the onmousedown and onmousenter
// events of all cells of the user schedule
// Parameters:
//  None 
// Returns:
//  Nothing
function attach_cell_listeners() {
    for(let row = 1; row <= 32; row++) {
        for(let column = 1; column <= 7; column++) {
            const cell = get_cell(row, column);
            cell.onmousedown = selection_start;   
            cell.onmouseenter = selection_continue;
        }
    }
}

// These global variables contain information about the user's current
// selection. Their not being null is taken to mean a selection is ongoing.

// A selection here is the process of pressing down the mouse button,
// draggin the cursor around, in and out of the schedule area,
// and finally lifting the mouse button

// During this process the area between the cell pressed first and the last cell
// entered by the cursor while the mouse button is down becomes highlighted.
// Once a selection is completed an AJAX request is sent to update the database
// records and display the changes in the group schedule
let start_row = null;
let start_column = null;
let end_row = null;
let end_column = null;
let new_selection = null;

// Starts the selection by recording the position of 
// the cell pressed and changing its color
// The selection starts when the user presses down on a cell
// Parameters:
//  event - the event object that is triggered. Gets used to
//          get access to the cell that launched the event
// Returns:
//  Nothing
function selection_start(event) {
    cell = event.target;
    start_row = end_row = cell.dataset.row;
    start_column = end_column = cell.dataset.column;
    new_selection = toggle_selection(cell);
}

// Continues the selection by adjusting the highlighted area
// and recording the cell hovered over
// The selection continues everytime the user drags the cursor
// to a new cell
// Parameters:
//  event - the event object that is triggered. Gets used to
//          get access to the cell that launched the event
// Returns:
//  Nothing
function selection_continue(event) {
    // If the mouse button is down
    if(event.buttons === 1) {
        // If new_selection is null that means the user is draggin
        // the cursor to a cell BUT didn't press the mouse button down
        // on a cell before that. In other words a selection hasn't begun
        if(new_selection != null) {
            change_selected_range(remove_highlight);
            cell = event.target;
            end_row = cell.dataset.row;
            end_column = cell.dataset.column;
            change_selected_range(add_highlight);
        }
    }
}

// Concludes the selection by removing the highlight on the selected
// area and triggering a AJAX request to record it
// The selection finishes once the user lifts the mouse button
// Parameters:
//  None
// Returns:
//  Nothing
function selection_end() {
    change_selected_range(remove_highlight);
    change_selected_range(set_selection);
    start_row = null;
    start_column = null;
    end_row = null;
    end_column = null;
    new_selection = null;
    // AJAX request
    vanilla_update();
}

// Performs a function on all cells in the selection
// The cells in the selection are those inside the rectangle
// between the cell pressed first and the last cell hovered over
// Parameters:
//  action - the function to apply to all cells
// Returns:
//  Nothing
function change_selected_range(action) {
    let first_row = Math.min(parseInt(start_row), parseInt(end_row));
    let last_row = Math.max(parseInt(start_row), parseInt(end_row));

    let first_column = Math.min(parseInt(start_column), parseInt(end_column));
    let last_column = Math.max(parseInt(start_column), parseInt(end_column));

    for(let row = first_row; row <= last_row; row++) {
        for(let column = first_column; column <= last_column; column++) {
            const cell = get_cell(row, column);
            action(cell);
        }
    }
}

// Changes the state of cell depending on its current state
// from unselected to selected and viceversa. These states are
// represented by css classes
// Parameters:
//  cell - the cell element to toggle
// Returns:
//  The state of the cell after being toggled.
//  This is the state that all cells will change to after
//  the selection
function toggle_selection(cell) {
    if(cell.classList.contains("unselected-cell")) {
        cell.classList.remove("unselected-cell");
        cell.classList.add("selected-cell");
        cell.classList.add("selected-highlighted-cell");
        return "selected-cell";
    }
    else {
        cell.classList.remove("selected-cell");
        cell.classList.add("unselected-cell");
        cell.classList.add("unselected-highlighted-cell");
        return "unselected-cell";
    }
}

// Removes all highlight css classes from a cell
// Parameters:
//  cell - the cell element to remove from
// Returns:
//  Nothing
function remove_highlight(cell) {
    if(cell.classList.contains("selected-highlighted-cell")) {
        cell.classList.remove("selected-highlighted-cell");
    }
    if(cell.classList.contains("unselected-highlighted-cell")) {
        cell.classList.remove("unselected-highlighted-cell");
    }
}

// Adds a highlight css class to a cell, matching the selection
// Parameters:
//  cell - the cell element to highlight
// Returns:
//  Nothing
function add_highlight(cell) {
    if(new_selection == "selected-cell") {
        cell.classList.add("selected-highlighted-cell");
    }
    else if(new_selection == "unselected-cell") {
        cell.classList.add("unselected-highlighted-cell");
    }
}

// Adds a css class to a cell to indicate its state
// according to the selection
// Parameters:
//  cell - the cell element to have classes added to it
// Returns:
//  Nothing
function set_selection(cell) {
    if(cell.classList.contains("unselected-cell")) {
        cell.classList.remove("unselected-cell");
    }
    if(cell.classList.contains("selected-cell")) {
        cell.classList.remove("selected-cell");
    }
    cell.classList.add(new_selection);
}

// Returns a cell element given its position
// Parameters:
//  row - an integer representing the row of the cell
//  column - an integer representing the column of the cell
// Returns:
//  The cell element on the specified position
function get_cell(row, column) {
    let cell_id = `cell-${row}-${column}-editable`;
    const cell = document.getElementById(cell_id);
    return cell;
}

// Returns true if a cell element is in the selected state
// as indicated by its css classes
// Parameters:
//  cell - the cell element to verify
// Returns:
//  True if the cell contains the css class of being selected
function is_selected(cell) {
    return (cell.classList.contains("selected-cell"));
}

// Sends an asynchronous request to update the content of the group schedule.
// After the request the group schedule will reflect the changes of the user
// Parameters:
//  None
// Returns:
//  Nothin
async function vanilla_update() {

    // The form to be sent with the request is a 2D array
    // with the user's just-finished selection. Every position
    // corresponds to a cell, with a 1 indicating that it was selected
    // and a 0 otherwise
    let parametros = new FormData();
    for(let row = 1; row <= 32; row++) {
        for(let column = 1; column <= 7; column++) {
            const cell = get_cell(row, column);
            if(is_selected(cell)) {
                // Notice the use of notation to indicate that what server
                // will receive is indeed a 2D array
                parametros.append(`selected_cells[${row}][${column}]`, 1);
            }
            else {
                parametros.append(`selected_cells[${row}][${column}]`, 0);
            }
        }
    }
    try {
        // Send asynchronous request
        await fetch('controllers/update_controller.php', {
            method: 'POST', // GET wouln't allow parameters
            body: parametros // The data that is sent with the request
            
        }).then(function (response) {
            return response.text(); 
        }).then(function (data) { 
            // Use request response text to update the group schedule
            document.getElementById("group-schedule").innerHTML = data;
        });
    } catch (e) {
        console.log(e);
        document.getElementById("").innerHTML = "Error en la comunicación con el servidor";
    }
}