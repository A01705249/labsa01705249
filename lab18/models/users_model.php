<?php
    // Include database utilities with absolute path
    $db_utils_absolute_path = $_SERVER['DOCUMENT_ROOT']."/labsa01705249/lab18/utils/db_utils.php";
    require_once($db_utils_absolute_path);

    // Returns the username and password of an user
    // Parameters:
    //  $username - the username to lookup
    // Returns:
    //  A query result object with the information of the requested user
    function get_user($username) {
        $connection = open_db_conection();

        $query = "SELECT username, password
                  FROM users 
                  WHERE username = ?";
        
        // Prepare parametrized query
        if(!$statement = $connection->prepare($query)) {
            die("Preparation error(" . $connection->error . "): " . $connection->error);
        }

        // Bind parameters
        if (!($statement->bind_param("s", $username))) {
            die("Binding error(" . $statement->error . "): " . $statement->error);
        }  

        // Run query
        if (!$statement->execute()) {
            die("Execution error(" . $statement->error . "): " . $statement->error);
        } 
        
        $query_result = $statement->get_result();
        close_db_conection($connection);
        return $query_result;
    }

    // Returns true if a user with a given username exists
    // Parameters:
    //  $username - the username to lookup
    // Returns:
    //  True if the user exists, false otherwise
    function user_exists($username) {
        $query_result = get_user($username);
        $user = mysqli_fetch_assoc($query_result);
        mysqli_free_result($query_result);

        return ($user !== null);
    }   

    // Returns true if the supplied username and passwords match
    // Parameters:
    //  $username - the supplied username
    //  $password - the password to verify against the user
    // Returns:
    //  True when the user exists and has the given password, false otherwise
    function valid_credentials($username, $password) {
        $query_result = get_user($username);
        $user = mysqli_fetch_assoc($query_result);
        mysqli_free_result($query_result);

        // If the user doesn't exist return false
        if($user == null) {
            return false;
        }

        if (strcmp($username, $user["username"]) ===  0  && 
            strcmp($password, $user["password"]) === 0) {
                return true;
        }
        return false;
    }

    // Register an user's info in the database
    // Parameters:
    //  $username - the username to record
    //  $password - the password to record
    // Returns:
    //  Nothing
    function register_user($username, $password) {
        $connection = open_db_conection();

        $query = "INSERT INTO users VALUES (?,?)";

        // Prepare parametrized query
        if(!$statement = $connection->prepare($query)) {
            die("Error(" . $connection->error . "): " . $connection->error);
        }

        // Bind parameters
        if (!($statement->bind_param("ss", $username, $password))) {
            die("Error de vinculación(" . $statement->error . "): " . $statement->error);
        }

        // Execute query
        if (!$statement->execute()) {
            die("Error en ejecución de la consulta(" . $statement->error . "): " . $statement->error);
        } 

        close_db_conection($connection);
    }

?>