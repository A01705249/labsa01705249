<?php
    // The session gets used to display the username on a header inside the content
    // and to pull the user's schedule from the database
    session_start();

    // Include schedule utilities with absolute path
    $schedule_utils_absolute_path = $_SERVER['DOCUMENT_ROOT']."/labsa01705249/lab18/utils/schedule_utils.php";
    require_once($schedule_utils_absolute_path);

    // If the user doesn't have a session, redirect to login page
    if (!isset($_SESSION["username"])) {
        header("Location: index.php");
    }

    include("statics/_header.html");
    include("statics/_navbar.html");

    // This is the partial that contains the page content
    include("statics/_schedule_body.php");

    include("statics/_footer.html");
?>