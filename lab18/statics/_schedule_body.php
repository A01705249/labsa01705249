<div class=" container-fluid" ondragstart="return false;" ondrop="return false;">
    <div class="row bg-white shadow rounded my-5 p-5">
        <div class="col-lg text-center">
        
            <div style="margin-left: 65px">
                <h4 class="mb-4"><?php echo "Disponibilidad de ".$_SESSION["username"]?></h4>
            </div>
            
            <div class="d-flex justify-content-center">
                <?=get_user_schedule_html()?>
            </div>
            
        </div>
        <div class="col-lg text-center">
            <div style="margin-left: 65px">
                <h4 class="mb-4">Disponibilidad del grupo</h4>
            </div>
            <!--The content of this div is the one that changes throug AJAX-->
            <div id="group-schedule" class="d-flex justify-content-center">
                <?=get_group_schedule_html()?>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript' src='controllers/schedule.js'></script>