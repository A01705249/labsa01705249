BULK INSERT a1705249.a1705249.[Proyectos]
FROM 'e:\wwwroot\a1705249\proyectos.csv'
WITH
(
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '0x0a'
)