BULK INSERT a1705249.a1705249.[Proveedores]
FROM 'e:\wwwroot\a1705249\proveedores.csv'
WITH
(
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '0x0a'
)