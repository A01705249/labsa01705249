BULK INSERT a1705249.a1705249.[Materiales]
FROM 'e:\wwwroot\a1705249\materiales.csv'
WITH
(
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '0x0a'
)