<?php
    session_start();

    include("statics/_header.html");
    include("statics/_navbar.html");

    if(isset($_SESSION["name"]) and isset($_SESSION["image"])) {
        include("statics/_schedule_selection.php");
    }
    else {
        include("statics/_data_required.html");
    }

    include("statics/_footer.html");
?>