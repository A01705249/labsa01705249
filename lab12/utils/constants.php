<?php
    // For this lab, server side validation is used on the form
    // The following constants are Boostrap classes that serve this purpose
    // They will be used on a controller to decide how an input should display
    // https://getbootstrap.com/docs/4.5/components/forms/#server-side
    const VALID_INPUT = "is-valid";
    const INVALID_INPUT = "is-invalid";  
?>