<?php
    // This funtion is used to sanitize some form inputs
    // It's a step in preventing XSS
    function cleanup_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    function rename_duplicates($path, $file) {   
        $file_name = pathinfo($path . $file, PATHINFO_FILENAME);
        $file_extension = "." . pathinfo($path . $file, PATHINFO_EXTENSION);
    
        $new_file_name = $path . $file_name . $file_extension;
        $copy = 1;
        
        while(file_exists($new_file_name)) {
            $new_file_name = $path . $file_name . '-copy-'. $copy . $file_extension;
            $copy++;
        }

        return $new_file_name;
    }
?>