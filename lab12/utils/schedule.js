window.onload = attach_listeners;
window.onmouseup = selection_end;

function attach_listeners() {
    for(let row = 1; row <= 32; row++) {
        for(let column = 1; column <= 5; column++) {
            const element = get_element(row, column);
            element.onmousedown = selection_start;   
            element.onmouseenter = selection_continue;
        }
    }
}

let start_row = null;
let start_column = null;
let new_selection = null;

function selection_start(event) {
    element = event.target;

    start_row = element.dataset.row;
    start_column = element.dataset.column;
    new_selection = toggle_selection(element);
}

function selection_continue(event) {
    if(event.buttons === 1) {
        if(new_selection != null) {
            element = event.target;

            end_row = element.dataset.row;
            end_column = element.dataset.column;
            
            let first_row = Math.min(parseInt(start_row), parseInt(end_row));
            let last_row = Math.max(parseInt(start_row), parseInt(end_row));

            let first_column = Math.min(parseInt(start_column), parseInt(end_column));
            let last_column = Math.max(parseInt(start_column), parseInt(end_column));

            for(let row = first_row; row <= last_row; row++) {
                for(let column = first_column; column <= last_column; column++) {
                    const element = get_element(row, column);
                    set_selection(element, new_selection);
                }
            }
        }
    }
}

function selection_end() {
    start_row = null;
    start_column = null;
    new_selection = null;
}

function toggle_selection(element) {
    if(element.classList.contains("unselected-cell")) {
        element.classList.remove("unselected-cell");
        element.classList.add("selected-cell");
        return "selected-cell";
    }
    else {
        element.classList.remove("selected-cell");
        element.classList.add("unselected-cell");
        return "unselected-cell";
    }
}

function set_selection(element, new_selection) {
    if(element.classList.contains("unselected-cell")) {
        element.classList.remove("unselected-cell");
    }
    if(element.classList.contains("selected-cell")) {
        element.classList.remove("selected-cell");
    }
    element.classList.add(new_selection);
}

function get_element(row, column) {
    const element_id = `cell-${row}-${column}`;
    const element = document.getElementById(element_id);
    return  element;
}

/*document.getElementById("MyElement").classList.add('MyClass');

document.getElementById("MyElement").classList.remove('MyClass');

if ( document.getElementById("MyElement").classList.contains('MyClass') )

document.getElementById("MyElement").classList.toggle('MyClass');*/