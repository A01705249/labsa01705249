<?php
    session_start();
    session_unset();
    session_destroy();

    include("statics/_header.html");
    include("statics/_navbar.html");

    include("statics/_exit_body.html");

    include("statics/_footer.html");
?>