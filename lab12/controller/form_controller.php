<?php
    $name_value = "";
    $name_class = "";
    $name_err_message = "";

    $file_value = "";
    $file_class = "";
    $file_err_message = "";

    if($_SERVER["REQUEST_METHOD"] == "POST") {
        $validation_passed = true;
        $validation_passed &= validate_name_input($name_value, $name_class, $name_err_message);
        $validation_passed &= validate_file_input($file_value, $file_class, $file_err_message);

        if($validation_passed) {
            $_SESSION["name"] = $name_value;
            $_SESSION["image"] = $file_value;
        }
    }

    function validate_name_input(&$name_value, &$name_class, &$name_err_message) {
        $name_value = cleanup_input($_POST["name"]);

        // check if name is empty
        if($name_value === "") {
            $name_err_message = "Por favor ingresa un nombre";
            $name_class = INVALID_INPUT;
            return false;
        } 
        
        // check if the name field only contains letters and whitespaces
        if (!preg_match("/^[a-zA-Z]*$/",$name_value)) {
            $name_err_message = "El nombre solo debe tener letras y espacios";
            $name_class = INVALID_INPUT;
            return false;
        }
        
        $name_class = VALID_INPUT;
        return true;    
    }

    function validate_file_input(&$file_value, &$file_class, &$file_err_message) {
        // Check if user selected a file
        if (empty($_FILES['fileToUpload']['name'])) {
            $file_err_message = "Por favor selecciona un archivo";
            $file_class = INVALID_INPUT;
            return false;
        }

        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            $file_err_message = "Por favor selecciona una imagen de menos de 500 KB";
            $file_class = INVALID_INPUT;
            return false;
        }
        
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            if($_FILES["fileToUpload"]["tmp_name"]) { // https://github.com/scotteh/php-goose/issues/57
                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                if(!($check !== false)) {
                    $file_err_message = "Por favor selecciona un archivo de imagen";
                    $file_class = INVALID_INPUT;
                    return false;
                }
            }   
            else {
                echo "
                <div class='alert alert-danger'>
                    Lo sentimos. Hubo un error al subir la imagen
                </div>";
                return false;
            }
        }

        // Change file name in case that there's already a file with the same name
        $target_dir = "uploads/";
        $target_file = rename_duplicates($target_dir, basename($_FILES["fileToUpload"]["name"]));

        // Allow certain file formats
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            $file_err_message = "Por favor selecciona una imagen de formato jpg, png, jpeg o gif";
            $file_class = INVALID_INPUT;
            return false;
        }
            
        if ( !(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) ) {
            echo "
            <div class='alert alert-danger'>
                Lo sentimos. Hubo un error al subir la imagen
            </div>";
            return false;
        }

        $file_value = $target_file;
        $file_class = VALID_INPUT;
        return true;
    }
?>