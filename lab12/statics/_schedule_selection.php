<div class="row container-fluid">
    <div class="col-sm-12 col-md-10 container bg-white shadow rounded my-5 p-5">
        <?php
        echo "<h1>Seccion abandonada</h1>";
        echo "<p>Sera completada y untilizada en el laboratorio <mark>18</mark></p>";
        echo "<table class='schedule-table'>";
        
        // Table heaver with abbreviations for days from monday to friday
        echo "<tr>";
        echo "<th></th>";
        echo "<th class='text-center'>Lun</th>";
        echo "<th class='text-center'>Mar</th>";
        echo "<th class='text-center'>Mie</th>";
        echo "<th class='text-center'>Jue</th>";
        echo "<th class='text-center'>Vie</th>";
        echo "</tr>";

        for($i = 700; $i <= 2250; $i+=50) {
            echo "<tr>";
            if($i % 100 == 0) {
                $time = floor($i/100);
                if($time > 12) {
                    $time -= 12; 
                    $time .= " PM";
                }
                else {
                    $time .= " AM";
                }
                echo "<td class='small text-right' rowspan='2'>".$time."</td>";
            }
            for($j = 1; $j <= 5; $j++) {
                $half_hour_class = "";
                if($i % 100 == 0) 
                    $half_hour_class = "first-half-schedule-cell";
                else
                    $half_hour_class = "second-half-schedule-cell";
                
                $row = (($i-700)/50)+1;
                $column = $j;
                $cell_id = "cell-".$row."-".$column;
                echo "<td id='".$cell_id."' class='schedule-cell unselected-cell ".$half_hour_class."' 
                data-row=".$row." data-column=".$column."></td>";
            }
            echo "</tr>";
        }
        echo "</table>";
        ?>
    </div>
</div>

<script type='text/javascript' src='utils/schedule.js'></script>