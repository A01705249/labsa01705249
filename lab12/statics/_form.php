<div class="row container-fluid">
    <div class="col-sm-12 col-md-8 col-lg-6 container bg-white shadow rounded my-5 p-4">
        <h1>Ingresa tus datos</h2>
        <p>Estos son los datos que indentificaran tu sesion</p>

        <!-- Form for the user to input a name and a file -->
        <form action="index.php" method="post" enctype="multipart/form-data" novalidate>
            <!-- Name input -->
            <div class="form-group mb-3">
                <label for="name-input">Nombre:</label>
                <input class="form-control <?php echo $name_class?>" 
                type="text" name="name" id="name-input" placeholder="Ingresa tu nombre" value="<?php echo $name_value;?>">
                <div class="invalid-feedback"><?php echo $name_err_message?></div>   
            </div>

            <!-- File input -->
            <div class="custom-file mb-3">  
                <input type="file" class="custom-file-input <?php echo $file_class?>" name="fileToUpload" id="fileToUpload">
                <label class="custom-file-label" for="fileToUpload" data-browse="Escoger">Escoge una foto...</label>
                <div class="invalid-feedback"><?php echo $file_err_message?></div>   
            </div>

            <!-- Submit button -->
            <div class="d-flex justify-content-center mt-3">
                <button type="submit" class="btn btn-success btn-lg" name="submit">Aceptar</button>
            </div>
        </form>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    