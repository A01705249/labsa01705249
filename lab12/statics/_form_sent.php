<div class="row container-fluid">
    <div class="col-sm-12 col-md-8 col-lg-6 container bg-white shadow rounded my-5 p-4">
        <h1>Datos guardados</h2>
        <p>Tus datos ya estan guardados. Presiona el boton de abajo para continuar a seleccionar tu horario</p>
        
        <div class="d-flex justify-content-center">
            <div class="card text-center mb-4" style="width: 10rem" >
                <img src="<?= $_SESSION['image']?>" class="card-img-top">
                <div class="card-body">
                    <h4 class="card-text"><?= $_SESSION["name"]?></h4>
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-center">
            <a href="schedule.php" class="btn btn-success">Seleccionar horario</a>
        </div>
    </div>
</div>

    