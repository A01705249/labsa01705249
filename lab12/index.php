<?php
    session_start();

    require_once("utils/functions.php");
    require_once("utils/constants.php");
    require_once("controller/form_controller.php");

    include("statics/_header.html");
    include("statics/_navbar.html");

    if(isset($_SESSION["name"]) and isset($_SESSION["image"])) {
        include("statics/_form_sent.php");
    }
    else {
        include("statics/_form.php");
    }
    
    include("statics/_footer.html");
?>