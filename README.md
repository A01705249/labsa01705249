# Repositorio de laboratorios de Desarollo de Aplicaciones Web y Bases de Datos

Este es el repositorio del alumno Adolfo Acosta (A01705249) para depositar los laboratorios propuestos en las materias de (TC2026)
Desarrollo de aplicaciones web y (TC1020) Bases de Datos, durante el semestre agosto - diciembre del 2020.

## Enlaces a las fuentes

- [Lab 1 - Introducción a las aplicaciones web, HTML5 y ciclo de vida de los sistemas de información](https://bitbucket.org/A01705249/labsa01705249/src/master/lab1/)

- [Lab 4 - Fundamentos de JavaScript](https://bitbucket.org/A01705249/labsa01705249/src/master/lab4/)

- [Lab 5 - Programación orientada a eventos](https://bitbucket.org/A01705249/labsa01705249/src/master/lab5/)

- [Lab 6 - Documentos dinámicos con JavaScript](https://bitbucket.org/A01705249/labsa01705249/src/master/lab6/)

- [Lab 7 - Front-end Frameworks ](https://bitbucket.org/A01705249/labsa01705249/src/master/lab7/)

- [Lab 8 - DMBS de escritorio](https://bitbucket.org/A01705249/labsa01705249/src/master/lab8/)

- [Lab 9 - Introducción a php](https://bitbucket.org/A01705249/labsa01705249/src/master/lab9/)

- [Lab 11 - Conociendo el ambiente de SQL Server](https://bitbucket.org/A01705249/labsa01705249/src/master/lab11/)

- [Lab 12 - Manejo de sesiones y subida de archivos con php](https://bitbucket.org/A01705249/labsa01705249/src/master/lab12/)

- [Lab 13 - Creación de Constraints Para Instrumentar Integridad Referencial en SQL Server](https://bitbucket.org/A01705249/labsa01705249/src/master/lab13/)

- [Lab 14 - php y consultas dinámicas a la base de datos](https://bitbucket.org/A01705249/labsa01705249/src/master/lab14/)

- [Lab 18 - Introduccion a AJAX](https://bitbucket.org/A01705249/labsa01705249/src/master/lab18/)

- [Lab 19 - Creación de Consultas Utilizando SQL con Funciones Agregadas y Sub-consultas](https://bitbucket.org/A01705249/labsa01705249/src/master/lab19/)

- [Lab 20 - jQuery](https://bitbucket.org/A01705249/labsa01705249/src/master/lab20/)

- [Lab 21 - Usabilidad](https://bitbucket.org/A01705249/labsa01705249/src/master/lab21/)

- [Lab 22 - Procedimiento almacenados](https://bitbucket.org/A01705249/labsa01705249/src/master/lab22/)

- [Lab 23 - Integración con servicios web](https://bitbucket.org/A01705249/labsa01705249/src/master/lab23/)

- [Lab 24 - Transacciones](https://bitbucket.org/A01705249/labsa01705249/src/master/lab24/)

  

## Datos del alumno
- Nombre: Adolfo Acosta Castro
- Matricula: A01705249
- Correo: A01705249@itesm.mx
