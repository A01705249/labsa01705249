// Tell the browser to assign my functions to the specified events after the page has loaded
window.onload = attachListeners;
function attachListeners() {
    document.getElementById("validation-button").onclick = validatePasswords;
    document.getElementById("visibility-button").onclick = togglePasswordVisibility;

    document.getElementById("game1-increase-button").onclick = changeProductCount;
    document.getElementById("game2-increase-button").onclick = changeProductCount;
    document.getElementById("game3-increase-button").onclick = changeProductCount;

    document.getElementById("game1-decrease-button").onclick = changeProductCount;
    document.getElementById("game2-decrease-button").onclick = changeProductCount;
    document.getElementById("game3-decrease-button").onclick = changeProductCount;
    
    document.getElementById("total-button").onclick = calculateTotal;

    document.getElementById("submit-button").onclick = displayVeridict;
}


// Checks that both passwords match and follow certain rules
// sends an alert with the result
function validatePasswords() {
    const password = document.getElementById("password").value;
    const confirmedPassword = document.getElementById("confirm-password").value;

    // Check for a minimum length of 8 
    const minPasswordLength = 8;
    if(password.length < minPasswordLength) {
        alert("The password must be at least 8 characters long");
        return;
    }

    // Check characters used in password
    let numberDigits = 0;
    let numberLowerCase = 0;
    let numberUpperCase = 0;
    let numberSpecial = 0;
    for(let i = 0; i < password.length; i++) {
        if('0' <= password[i] && password[i] <= '9') 
            numberDigits++;
        else if('a' <= password[i] && password[i] <= 'z') 
            numberLowerCase++;
        else if('A' <= password[i] && password[i] <= 'Z')
            numberUpperCase++;
        else 
            numberSpecial++;
    }

    // Check for at least 1 of each: lower case letters, upper case letters, digits and speacial characters
    if(numberLowerCase === 0) {
        alert("The password must contain at least one lower case letter");
        return;
    }
    if(numberUpperCase === 0) {
        alert("The password must contain at least one uppers case letter");
        return;
    }
    if(numberDigits === 0) {
        alert("The password must contain at least digit");
        return;
    }
    if(numberSpecial === 0) {
        alert("The password must contain at least one special character");
        return;
    }

    // Check that both passwords match
    if(password !== confirmedPassword) {
        alert("The passwords do not match. Please verify your input");
        return;
    }

    // If code reaches this point everything, password was alright
    alert("Password has been validated. All ok")
}

// Shows or hides the content of the password textboxes and changes the
// eye icon depending on their current state
function togglePasswordVisibility() {
    const passwordField = document.getElementById("password");
    const confirmField = document.getElementById("confirm-password");
    const visibilityButton = document.getElementById("visibility-button");

    if(passwordField.type === "password") {
        passwordField.type = "text";
        confirmField.type = "text";
        visibilityButton.src = "images/eye-open.png";
    }
    else {
        passwordField.type = "password";
        confirmField.type = "password";
        visibilityButton.src = "images/eye-shut.png";
    }
}

// Changes the quantities shown for products in the sales section
// disables the decrease button to prevent the user from selecting less than 0 items
function changeProductCount() {
    const quatityContainer = this.parentElement.parentElement
    const quantityBox = quatityContainer.firstElementChild;
    let currentQuantity = parseInt(quantityBox.value);
    const decreaseButton = quatityContainer.children[1].children[1];

    if(this.name === "increase") {
        currentQuantity++;
        decreaseButton.disabled = false;
    }
    else {
        currentQuantity--;
        if(currentQuantity === 0) {
            decreaseButton.disabled = true;
        }
    }

    quantityBox.value = currentQuantity.toString();
}

// Shows an alert with the calculated price for the selected items
function calculateTotal() {
    const quatity1 = parseInt(document.getElementById("game1-quantity-box").value);
    const quatity2 = parseInt(document.getElementById("game2-quantity-box").value);
    const quatity3 = parseInt(document.getElementById("game3-quantity-box").value);
    
    quantities = [quatity1, quatity2, quatity3];
    prices = [109.99, 529.00, 891.26];
    
    const IVA = .16;
    let total = 0;
    for(let i = 0; i < 3; i++) {
        total += quantities[i] * prices[i];
    }

    total += total * IVA; //Add IVA
    total = Math.round(total*100)/100; // Truncate to 2 decimals

    alert(`El total es: MXN$${total}`);
}

// Sends an alert accepting the aplication if the user is un Queretaro 
// or willing to relocate otherwise rejects them
function displayVeridict() {
    const name = document.getElementById("full-name").value;
    const mail = document.getElementById("email").value;
    
    const genderOptions = document.getElementsByName("gender");
    let gender;
    for(let i = 0; i < genderOptions.length; i++) {
        if(genderOptions[i].checked) {
            gender = genderOptions[i].value;
        }
        
    }

    stateSelect = document.getElementById("state");
    const state = stateSelect.options[stateSelect.selectedIndex].value;

    const availableCheckbox = document.getElementById("available");
    let canRelocate = availableCheckbox.checked;

    let title = "";
    if(gender === "man") 
        title = "Sr.";
    else 
        title = "Srta.";
    
    let desition = "";
    if(canRelocate || state === "QRO")
        desition = `Estaremos en contacto con usted a traves de su correo ${mail}.`;
    else
        desition = "Parece que no podemos trabajar con usted en este momento."

    let message = `Gracias por su interes ${title} ${name}. ` + desition;

    alert(message);
}