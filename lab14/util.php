<?php
    // Opend a conection to a database
    // Parameters:
    //  None, they are hardcoded for the exercise
    // Returns:
    //  A conection object
    function openDbConection() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "empleados";
        
        // Create conection
        $conection = mysqli_connect($servername, $username, $password, $dbname);

        // Check connection
        if (!$conection) {
            die("Connection failed: " . mysqli_connect_error());
        }

        return $conection;
    }

    // Closes a previosly opened database conection
    // Parameters:
    //  $conection - The previously opened database conection object
    // Returns:
    //  Nothing
    function closeDbConection($conection) {
        mysqli_close($conection);
    }

    // Queries the database for employee data
    // Parameters:
    //  None
    // Returns:
    //  The result of the call to mysqli_query
    function getEmployeeData() {
        $conection = openDbConection();
        $query = "SELECT empleado.nombre as nombre, correo, genero, puesto.nombre as puesto, sueldo FROM empleado, puesto WHERE empleado.idPuesto = puesto.idPuesto";
        $result = mysqli_query($conection, $query);
        closeDbConection($conection);
        return $result;
    }

    // Generates the HTMl for a table showing the result of a call to getEmployeeData
    // This is function that's meant to be used in index_body.php as it keeps us from
    // having to specify parameters in that file
    function employeeTable() {
        $query_result = getEmployeeData();
        $headingNames = array("Nombre", "Correo", "Género", "Puesto", "Sueldo");
        $columnNames = array("nombre", "correo", "genero", "puesto", "sueldo");
        return getTable($headingNames, $columnNames, $query_result);
    }


    // Queries the database for employee data of a given position
    // Parameters:
    //  $Position - The position of the employees that will be returned
    // Returns:
    //  The result of the call to mysqli_query
    function getEmployeesByPosition($position) {
        $conection = openDbConection();
        $query = "SELECT empleado.nombre as nombre, correo, genero FROM empleado, puesto WHERE empleado.idPuesto = puesto.idPuesto AND puesto.nombre = '".$position."'";
        $result = mysqli_query($conection, $query);
        closeDbConection($conection);
        return $result;
    }     

    // Generates the HTMl for a table showing the result of a call to getEmployeesByPosition
    // This is function that's meant to be used in index_body.php as it keeps us from
    // having to specify parameters in that file
    function profesorsTable() {
        $query_result = getEmployeesByPosition("Profesor");
        $headingNames = array("Nombre", "Correo", "Género");
        $columnNames = array("nombre", "correo", "genero");
        return getTable($headingNames, $columnNames, $query_result);
    }


    // Queries the database for data of employees with salary above a certain threshold
    // Parameters:
    //  $salary - The minimum salary for an employee to appear in the query.                     Employees with  
    // Returns:
    //  The result of the call to mysqli_query
    function getHigherSalaryEmployees($salary) {
        $conection = openDbConection();
        $query = "SELECT empleado.nombre as nombre, correo, genero, puesto.nombre as puesto, sueldo FROM empleado, puesto WHERE empleado.idPuesto = puesto.idPuesto AND sueldo >= ".$salary;
        $result = mysqli_query($conection, $query);
        closeDbConection($conection);
        return $result;
    }     

    // Generates the HTMl for a table showing the result of a call to getHigherSalaryEmployees
    // This is function that's meant to be used in index_body.php as it keeps us from
    // having to specify parameters in that file
    function higherSalaryTable() {
        $query_result = getHigherSalaryEmployees("300000");
        $headingNames = array("Nombre", "Correo", "Género", "Puesto", "Salary");
        $columnNames = array("nombre", "correo", "genero", "puesto", "sueldo");
        return getTable($headingNames, $columnNames, $query_result);
    }
    

    // Generates the HTML for a table containing all rows of a query result
    // 
    // Parameters:
    //  $headingNames - an array with strings representing the names of the columns that
    //                  that will be shown inside the <th> elements. Should be
    //  $columnNames -  an array with strings representing the names of the columns of
    //                  of $query_result
    //  $query_result - the object returned by a mysqli_query 
    //
    // Returns:
    //  A string of HTML code with a formatted table showing all rows of $query_result
    function getTable($headingNames, $columnNames, $query_result) {
        $tableHTML = "<table class='table table-striped table-bordered table-hover'>";
        
        // Table heading
        $tableHTML .= "<thead>";
        $tableHTML .= " <tr>";
        for($i = 0; $i < count($headingNames); $i++) {
            $tableHTML .= "  <th>".$headingNames[$i]."</th>";
        }
        $tableHTML .= " </tr>";
        $tableHTML .= "</thead>";
        
        // Table body
        $tableHTML .= "<tbody>";
        
        while ($row = mysqli_fetch_array($query_result, MYSQLI_ASSOC)) { 
            $tableHTML .= '<tr>';
            for($i = 0; $i < count($columnNames); $i++) {
                $tableHTML .= "  <td>".$row[$columnNames[$i]]."</td>";
            }
            $tableHTML .= '</tr>';
        }

        $tableHTML .= "</tbody>";
        $tableHTML .= "</table>";
        
        // Free memory
        mysqli_free_result($query_result); 

        return $tableHTML;
    }
?>