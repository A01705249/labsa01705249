<div class="row container-fluid mx-0 my-5">
    <div class="col-12 container bg-white shadow rounded p-5">
        <h1>Vistas de consultas</h1>
        <p>Con el siguiente esquema de base de datos, se hacen consultas y se despliegan sus resultados en las siguientes tablas</p>
        <img src="statics/diagram.png" class="img-thumbnail mx-auto d-block"> 
        <ul class="list-group list-group-flush">
            <li class="list-group-item mt-4">
                <h5>Datos de todos los empleados</h5>
                <p>Se muestran los datos de todos los empleados incluyendo la informacion sobre su puesto</p>
                <?=employeeTable()?>
            </li>
            <li class="list-group-item mt-4">
                <h5>Datos de profesores</h5>
                <p>Se muestra la informacion de los empleados que son profesores</p>
                <?=profesorsTable()?>
            </li>
            <li class="list-group-item mt-4">
                <h5>Datos de empleados con sueldo sobre un limite</h5>
                <p>Se muestra la informacion de los empleados con sueldo mayor a 300,000</p>
                <?=higherSalaryTable()?>
            </li>
        </ul> 
    </div>
</div>