-- Lab 22 Manipulación de datos usando Stored Procedures
-- Alumno: Adolfo Acosta Castro
-- Matricula: A01705249
-- En este archivo se crean los procedimientos almacenados
-- que se proponen en este laboratorio

-- Insertar un material nuevo
CREATE PROCEDURE creaMaterial
    @uclave NUMERIC(5,0),
    @udescripcion VARCHAR(50),
    @ucosto NUMERIC(8,2),
    @uimpuesto NUMERIC(6,2)
AS
    INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
GO

-- Modificar un material 
CREATE PROCEDURE modificaMaterial
    @clave NUMERIC(5,0),
    @descripcion VARCHAR(50),
    @costo NUMERIC(8,2),
    @impuesto NUMERIC(6,2)
AS
    UPDATE Materiales 
	SET Descripcion =  @descripcion, Costo = @costo, PorcentajeImpuesto = @impuesto
	WHERE Clave = @clave
GO

-- Borrar un material
CREATE PROCEDURE eliminaMaterial
    @clave NUMERIC(5,0)
AS
    DELETE FROM Materiales WHERE Clave = @clave
GO








-- Insertar un proyecto nuevo
CREATE PROCEDURE creaProyecto
    @numero NUMERIC(5,0),
    @denominacion VARCHAR(50)
AS
    INSERT INTO Proyectos VALUES(@numero, @denominacion)
GO

-- Modificar un proyecto 
CREATE PROCEDURE modificaProyecto
    @numero NUMERIC(5,0),
    @denominacion VARCHAR(50)
AS
    UPDATE Proyectos 
	SET Denominacion =  @denominacion
	WHERE Numero = @numero
GO

-- Borrar un proyecto
CREATE PROCEDURE eliminaProyecto
    @numero NUMERIC(5,0)
AS
    DELETE FROM Proyectos WHERE Numero = @numero
GO







-- Insertar un proveedor nuevo
CREATE PROCEDURE creaProveedor
    @rfc char(13),
    @razonSocial VARCHAR(50)
AS
    INSERT INTO Proveedores VALUES(@rfc, @razonSocial)
GO

-- Modificar un proveedor 
CREATE PROCEDURE modificaProveedor
    @rfc char(13),
    @razonSocial VARCHAR(50)
AS
    UPDATE Proveedores 
	SET RazonSocial =  @razonSocial
	WHERE RFC = @rfc
GO

-- Borrar un proveedor
CREATE PROCEDURE eliminaProveedor
    @rfc char(13)
AS
    DELETE FROM Proveedores WHERE RFC = @rfc
GO






-- Insertar una entrega nueva
CREATE PROCEDURE creaEntrega
	@clave NUMERIC(5,0),
    @rfc char(13),
	@numero NUMERIC(5,0),
    @fecha datetime,
	@cantidad NUMERIC(8,2)
AS
    INSERT INTO Entregan VALUES(@clave, @rfc, @numero, @fecha, @cantidad)
GO

-- Modificar una entrega 
CREATE PROCEDURE modificaEntrega
    @clave NUMERIC(5,0),
    @rfc char(13),
	@numero NUMERIC(5,0),
    @fecha datetime,
	@cantidad NUMERIC(8,2)
AS
    UPDATE Entregan 
	SET Cantidad =  @cantidad
	WHERE Clave = @clave AND RFC = @rfc AND
		  Numero = @numero AND @fecha = Fecha
GO

-- Borrar una entrega
CREATE PROCEDURE eliminaEntrega
    @clave NUMERIC(5,0),
    @rfc char(13),
	@numero NUMERIC(5,0),
    @fecha datetime
AS
    DELETE FROM Entregan 
	WHERE Clave = @clave AND RFC = @rfc AND
		  Numero = @numero AND @fecha = Fecha
GO



IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'queryMaterial' AND type = 'P')
    DROP PROCEDURE queryMaterial
GO

CREATE PROCEDURE queryMaterial
    @udescripcion VARCHAR(50),
    @ucosto NUMERIC(8,2)

AS
    SELECT * FROM Materiales WHERE descripcion
    LIKE '%'+@udescripcion+'%' AND costo > @ucosto
GO

EXECUTE queryMaterial 'Lad',20