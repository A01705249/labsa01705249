function exercise1() {
    let lim = prompt("Introduce un entero positivo");
    document.write("<table>");
    for(let i = 1; i <= lim; i++) {
        document.write("<tr>");
        document.write("<td>"+i+"</td>"+"<td>"+i*i+"</td>"+"<td>"+i*i*i+"<td>");
        document.write("</tr>");
    }
    document.write("</table>");
}

function exercise2() {
    let num1 = Math.floor(Math.random() * 6);
    let num2 = Math.floor(Math.random() * 6);
    let answer = num1 + num2;

    let start = Date.now();
    let guess = prompt("Adivina la suma de dos enteros aleatorios, ambos entre 0 y 5");
    let duration = (Date.now() - start) / 1000;

    if(guess == answer) {
        document.write("<h1>Correcto</h1>");
    }
    else {
        document.write("<h1>Incorrecto</h1>");
    }
    document.write(`<p>Adivinaste ${guess}</p>`);
    document.write(`<p>La respuesta era ${answer}</p>`);
    document.write(`<p>Te tomo ${duration} segundos dar tu respuesta</p>`);
}

function count_signed_ocurrences(arr) {
    let neg = 0;
    let zero = 0;
    let pos = 0;
    for(let i = 0; i < arr.length; i++) {
        if(arr[i] < 0) {
            neg++;
        }
        else if(arr[i] === 0) {
            zero++;
        }
        else {
            pos++;
        }
    }
    let ans = [neg, zero, pos];
    return ans;
}

function exercise3() {
    const arr1 = [-12, 1, 0, 1000, 0.34, -69, 0, 0];
    const arr2 = [-1, 0, 0, 1, 2, 3];
    
    document.write(`<p>El contenido de arr1: ${arr1}</p>`);
    document.write(`<p>El resultado de la funcion con arr1: ${count_signed_ocurrences(arr1)}</p>`);

    document.write(`<p>El contenido de arr2: ${arr2}</p>`);
    document.write(`<p>El resultado de la funcion con arr2: ${count_signed_ocurrences(arr2)}</p>`);
}

function row_average(matrix) {
    let rows = matrix.length;
    let averages = [];
    for(let i = 0; i < rows; i++) {
        let columns = matrix[i].length;
        let sum = 0;
        for(let j = 0; j < columns; j++) {
            sum += matrix[i][j];
        }
        let average = sum / columns;
        averages.push(average);
    }
    return averages;
}

function exercise4() {
    const matrix1 = [[1,2,3],[4,5,6],[7,8,9]];
    const matrix2 = [[11,45,70],[28,97,2],[0,86,31]];
    
    document.write(`<p>El contenido de matrix1: ${matrix1}</p>`);
    document.write(`<p>El resultado de la funcion con arr1: ${row_average(matrix1)}</p>`);

    document.write(`<p>El contenido de matrix2: ${matrix2}</p>`);
    document.write(`<p>El resultado de la funcion con arr2: ${row_average(matrix2)}</p>`);
}


function reverse_digits(num) {
    let inv = 0;
    while(num > 0) {
        let least_num_digit =  num % 10;
        num = Math.floor(num / 10);
        inv = (inv * 10) + least_num_digit;
    }
    return inv;
}

function exercise5() {
    const num1 = 123;
    const num2 = 123456789;
    document.write(`<p> El numero ${num1} al reves se ve asi: ${reverse_digits(num1)}</p>`);
    document.write(`<p> El numero ${num2} al reves se ve asi: ${reverse_digits(num2)}</p>`);
}

class Random {
    constructor() {
        // You'd initialize the object here
    }
    // Returns a random integer in the range [min, max]
    static randomInteger(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
    // Returns a random string made up of lower case letter of length len
    static randomAlphaString(len) {
        const alphabet="qwertyuiopasdfghjklzxcvbnm";
        let res = "";
        while(len > 0) {
            const position = this.randomInteger(1,26);
            res += alphabet.charAt(position);
            len--;
        }
        return res;
    }
}

function exercise6() {
    // Here's how you'd create an object, but this class' methods are
    // intended to be used without any instances, something enable by the static modifier
    const rand = new Random(); 

    document.write(`<p>Un numero aleatorio (obtenido con una funcion conveniente): ${Random.randomInteger(-100,100)}</p>`);
    document.write(`<p>Una cadena aleatoria de letras minusculas: ${Random.randomAlphaString(10)}</p>`);
}