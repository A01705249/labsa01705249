<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='styles.css' rel='stylesheet' type='text/css' />

    <title>Lab 9</title>
</head>
<body>

    <header class="document-header">
        <h1 id="lab-title">Laboratorio 9</h1>
        <p>Introduccion a php</p>
    </header>

    <!--PHP exercises-->
    <section>
        <header><h2>Ejercicios basicos php</h2></header>
        
        <div class="exercise-container">
            <h3>Datos de entrada</h3>
            <?php
                require_once("util.php");
                $array1 = array(1,2,3,4,5,6,7,8,9,10);
                $array2 = array(150, 50, 100);
                echo "Arreglo 1:";
                show_array($array1);
                echo "Arreglo 2:";
                show_array($array2);
            ?>
        </div> 

        <div class="exercise-container">
            <h3>Promedio de un arreglo</h3>
            <?php
                echo "<p>"."Arreglo 1: ".average($array1)."</p>";
                echo "<p>"."Arreglo 2: ".average($array2)."</p>";
            ?> 
        </div> 

        <div class="exercise-container">
            <h3>Mediana de un arreglo</h3>
            <?php
                echo "<p>"."Arreglo 1: ".median($array1)."</p>";
                echo "<p>"."Arreglo 2: ".median($array2)."</p>";
            ?> 
        </div>  

        <div class="exercise-container">
            <h3>Valores varios sobre un arreglo formateados en lista</h3>
            <?php
            array_info($array1);
            echo "<hr>";
            array_info($array2);
            ?> 
        </div> 
        
        <div class="exercise-container">
            <h3>Cuadrados y cubos formateados en tabla</h3>
            <?php
            squares_cubes_table(3);
            echo "<hr>";
            squares_cubes_table(5);
            ?> 
        </div> 

        <div class="exercise-container">
            <h3>Usar un hash de php</h3>
            <form method="POST">
                <label for="hash-textbox">Ingresa tu nombre</label>
                <input id="hash-textbox" type="text" name="name">
                <input type="submit" value="Submit Name">
            </form>
            <?php
            $name = $_POST['name'];
            echo "<p> El nombre: $name</p>";
            echo "<p> El nombre hasheado: ".hash("sha1", $name)."</p>";
            ?>
        </div>

    </section>
        
    <!--Questions, answers and reference-->
    <section class="question-section">
        <header><h2>Preguntas a responder</h2></header>
        <ul>
            <li class="question-answer-item">
                <h3>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</h3>
                <p>Genera una seccion con informacion sobre la configuracion de PHP. Estos son algunas de las secciones que contiene:</p>
                <ul>
                    <li>Hash: menciona varios algoritmos de hasheado disponibles y si su uso se esta soportado.</li>
                    <li>HTTP request: muestra el metodo http usado y otros datos de cabecera.</li>
                    <li>Session support: se refiere a un mecanismo de php para mantener informacion a lo largo de varios accesos.</li>
                </ul>
            </li>
            <li class="question-answer-item">
                <h3>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</h3>
                <p>Cambios relacionados con seguridad, como que privilegios se requieren para acceder a ciertos recursos y funciones del servidor.</p>
            </li>
            <li class="question-answer-item">
                <h3>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</h3>
                <p>Cuando el servidor recibe una peticion de un archivo de php primero se pasa por el compilador de php, este procesa el codigo y cuando termina los resultados son enviados y desplegados por el navegador del lado del cliente.</p>
            </li>
        </ul>
        <footer>
            <h3>Referencias</h3>
            <ul>
                <li><a href="https://www.php.net/manual/en/function.phpinfo">phpinfo</a></li>
                <li><a href="https://www.php.net/manual/en/intro.session.php">Sesiones de php</a></li>
                <li><a href="https://www.php.net/manual/en/book.hash.php">Hashes en php</a></li>
                <li><a href="https://documentation.progress.com/output/ua/OpenEdge_latest/index.html#page/pasoe-admin/development-servers-compared-to-production-serve.html">Development servers compared to production servers</a></li>
                <li><a href="https://stackoverflow.com/questions/21373242/how-is-a-php-file-executed">Como se ejecuta un archivo de php</a></li>
            </ul>
        </footer>
    </section>

</body>
</html>