<?php

function show_array($array) {
    echo '<p>';
    foreach ($array as &$value) {
        echo $value.' ';
    }
    echo '</p>';
    unset($value); // break the reference with the last element
}

function average($array) {
    if(sizeof($array) === 0) {
        return 0;
    }
    return array_sum($array)/sizeof($array);
}

function median($array) {
    sort($array);
    $count = sizeof($array); 
    $index = floor($count/2); 
    if($count === 0) {
        return 0;
    } 
    else if(($count % 2) === 1) {    
        return $array[$index];
    } 
    else {                  
        return ($array[$index-1] + $array[$index]) / 2;
    }
}

function array_info($array) {
    echo '<ul>';
    echo '<li> Valores del arreglo: ';show_array($array); echo '</li>';
    echo '<li> Promedio: '.average($array).'</li>';
    echo '<li> Mediana: '.median($array).'</li>';
    sort($array);
    echo '<li> Valores en orden ascendente: ';show_array($array); echo'</li>';
    echo '<li> Valores en orden decendente: ';show_array(array_reverse($array)); echo '</li>';
    echo '</ul>';
}

function squares_cubes_table($limit) {
    echo '<table>';
    for($i = 1; $i <= $limit;  $i++) {
        echo '<tr>';
        echo '<td>'.$i*$i.'</td>';
        echo '<td>'.$i*$i*$i.'</td>';
        echo '</tr>';
    }
    echo '</table>';
}

?> 