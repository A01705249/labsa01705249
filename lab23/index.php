<?php
  // Cargar las librerias y las variables de entorno
  require "prep.php"; 
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lab 23: Google “No CAPTCHA reCAPTCHA”</title>

  </head>
 
  <body>

    <h1>Prueba de consumo de servicio web reCAPTCHA</h1>
    <p>Debajo se utiliza un <a href="https://developers.google.com/recaptcha/docs/versions#recaptcha_v2_im_not_a_robot_checkbox">reCAPTCHA v2 ("No soy un robot" Checkbox)</a> para proteger un formulario simple de spam.</p>
    <br>
    <form action="backend.php" method="post">
 
      <label for="name">Name:</label>
      <input name="name" required><br/>
 
      <label for="email">Email:</label>
      <input name="email" type="email" required><br/>

      <!-- Este es el contenedor donde se generara el reCaptcha -->
      <!-- Notese que contiene la "site key" del par de llaves proporcionadas por Google 
           al registrarse para consumir este servicio -->
      
      <div class="g-recaptcha" data-sitekey="<?=$_ENV["RECAPTCHA_SITE_KEY"]?>"></div>
 
      <input type="submit" value="Submit" />
 
    </form>
  
    <h2>¿Qué ventajas y desventajas tiene la integración de tus aplicaciones web con servicios web desarrollados por terceros?</h2>
    <p>Por un lado esta la posibilidad de utilizar todo un conjunto de funcionalidad que ya existe, ahorrandote el trabajo de hacerlo desde cero. En el caso de desarrolladores confiables se puede esperar un servicio de gran calidad y habra una cantidad extensa de documentacion, ejemplos y una comunidad que consultar. Con desarrolladores mas chicos hay menor garantia de que esto ocurra. Ademas de que en cualquier caso utilizar uno de estos servicios involucra estar a la merced de la disponibilidad e interfaz que ofrezcan.</p>
    <!--JavaScript para cargar el componente del formulario -->
    <!-- Notese la opcion para ajustar el idioma -->
    <script src="https://www.google.com/recaptcha/api.js?hl=es"></script>
 
  </body>
</html>