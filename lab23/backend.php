<?php
    // Cargar las librerias y las variables de entorno
    require "prep.php";

    // Esta es la "secret key" que se utiliza para validar la respuesta del usuario del lado del servidor
    $secretKey = $_ENV["RECAPTCHA_SECRET_KEY"];

    // Si se recibe el campo del recaptcha, verificar su valor
    if (isset($_POST['g-recaptcha-response'])) {
        // Este es un ejemplo, por eso los datos del formulario no se sanitizan o validan

        // Crear una instancia del servicio usando la llave     
        $reCaptcha = new \ReCaptcha\ReCaptcha($secretKey);

        // Hacer llamada para verificar respuesta del usuario y tambien pasar ip del usuario
        $user_response = $_POST['g-recaptcha-response'];
        $user_ip = $_SERVER['REMOTE_ADDR'];
        $reCaptcha->setExpectedHostname($_SERVER['SERVER_NAME']);
        $response = $reCaptcha->verify($user_response, $user_ip);

        if ($response->isSuccess()) {
            // Si la respuesta indica a la llamada verify indica exito, eso es todo
            echo '<h2>Exito!</h2>';
            echo "<p>Hola " . $_POST["name"] . " (" . $_POST["email"] . "), gracias por enviar el formulario!</p>";
            echo '<kbd><pre>'.var_export($response).'</pre></kbd>';
            echo '<p><a href="index.php">⤴️ Intentar de nuevo</a></p>';
            
        }
        else {
            // Si la llamada no fue exitosa, entonces uno o varios codigos de error son retornados
            echo '<h2>Algo fallo</h2>';
            echo '<kbd><pre>'.var_export($response).'</pre></kbd>';
            echo '<p>Checa la referencia de codigos de error en <kbd><a href="https://developers.google.com/recaptcha/docs/verify#error-code-reference">https://developers.google.com/recaptcha/docs/verify#error-code-reference</a></kbd>.';
            echo '<p><a href="index.php">⤴️ Intentar de nuevo</a></p>';
        }
    }
    else {
        header("Location: index.php");
    }
?>