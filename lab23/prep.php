<?php
    // Cargar libreria de reCAPTCHA y de carga de variables de entorno
    require __DIR__ . '/vendor/autoload.php';

    // Cargar las variables de entorno en el archivo .env
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
?>