window.onload = setUpPuzzle;
function setUpPuzzle() {
    for(let i = 1; i <= 3; i++) {
        for(let j = 1; j <= 3; j++) {
            const elementId = `row-${i}-col-${j}`;
            const element = document.getElementById(elementId);
            // element.draggable = true; Images are draggable  by default
            element.ondragstart = drag;

            const elementContainer = element.parentElement;
            elementContainer.ondragover = allowDrop;
            elementContainer.ondrop = drop;
        }
    }
    scramblePuzzle();
}

function swapChildren(parentA, childOfA, parentB, childOfB) {
    parentA.appendChild(childOfB);
    parentB.appendChild(childOfA);
}

// Returns a random integer in [1,3]
function randPuzzleIndex() {
    return (Math.floor(Math.random()*3)+1);
}

function scramblePuzzle() {
    for(let i = 1; i <= 100; i++) {
        const element1 = document.getElementById(`row-${randPuzzleIndex()}-col-${randPuzzleIndex()}`);
        const element2 = document.getElementById(`row-${randPuzzleIndex()}-col-${randPuzzleIndex()}`);
        const parent1 = element1.parentElement;
        const parent2 = element2.parentElement;
        swapChildren(parent1, element1, parent2, element2);
    }
}

function drag(event) {
    event.dataTransfer.setData("text", event.target.id);
}

function allowDrop(event) {
    event.preventDefault();
}

// Not sure why this event gets called on the <img> that you drop on instead of its sorrounding <td>
function drop(event) {
    event.preventDefault();
    var data = event.dataTransfer.getData("text");
    
    const draggedElement = document.getElementById(data);
    const targetElement = event.target;

    const targetParent = targetElement.parentElement;
    const draggedParent = draggedElement.parentElement;

    swapChildren(targetParent, targetElement, draggedParent, draggedElement);
    targetParent.appendChild(draggedElement);
    draggedParent.appendChild(targetElement);

    if(puzzleSolved()) {
        disabledDragDrop();
        reStylePuzzle();
        startGlowAnimation();
    }
}   

function puzzleSolved() {
    for(let i = 1; i <= 3; i++) {
        for(let j = 1; j <= 3; j++) {
            const elementId = `row-${i}-col-${j}`;
            const element = document.getElementById(elementId);
            const elementContainerId = element.parentElement.id;
           
            if(elementId.concat("-container") !== elementContainerId) {
                return false;
            }
        }
    }
    return true;
}

function disabledDragDrop() {
    for(let i = 1; i <= 3; i++) {
        for(let j = 1; j <= 3; j++) {
            const elementId = `row-${i}-col-${j}`;
            const element = document.getElementById(elementId);
            element.draggable = false;
        }
    }
}

function reStylePuzzle() {
    for(let i = 1; i <= 3; i++) {
        for(let j = 1; j <= 3; j++) {
            const elementId = `row-${i}-col-${j}`;
            const element = document.getElementById(elementId);
            element.style.borderRadius = "0px";
            element.style.border = "0px";
        }
    }
}

var upperShadowLimit = 20;
var lowerShadowLimit = 0;
var shadowPosition = 0;
var increment = 1;

function startGlowAnimation() {
    setInterval(animateGlow, 50);
}

function animateGlow() {
    shadowPosition += increment;
    if(shadowPosition == upperShadowLimit || shadowPosition == lowerShadowLimit) 
        increment *= -1;
        
    let sp = shadowPosition;
    let blur = sp+10;
    element = document.getElementById("puzzle-container");
    element.style.boxShadow = `${sp}px ${sp}px ${blur}px rgb(255, 177, 10), 
                               -${sp}px -${sp}px ${blur}px rgb(255, 177, 10),
                               -${sp}px ${sp}px ${blur}px rgb(255, 177, 10),
                               ${sp}px -${sp}px ${blur}px rgb(255, 177, 10)`;
    
}