<?php
    require_once("models/model.php");
    require_once("utils/db_utils.php");
    require_once("controllers/registro_actualizacion_prep.php");

    include("statics/_header.html");
    include("statics/_navbar.html");

    include("statics/_registro_actualizacion_body.html");

    include("statics/_footer.html");
?>