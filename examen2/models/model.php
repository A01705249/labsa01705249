<?php
    function register_new_evolution($id_zombie, $id_estado) {
        $connection = open_db_conection();
        $sql = "INSERT INTO zombie_estado VALUES (?, ?, CURRENT_TIMESTAMP())"; 
                
        if (!($statement = $connection->prepare($sql))) 
            die("Error(" . $connection->error . "): " . $connection->error);
        if (!($statement->bind_param("ii", $id_zombie, $id_estado))) 
            die("Error de vinculación(" . $statement->error . "): " . $statement->error);
        if (!$statement->execute()) 
            die("Error en ejecución de la consulta(" . $statement->error . "): " . $statement->error);

        $result = $statement->get_result();
        close_db_conection($connection);
    }

    function register_new_zombie($name) {
        $connection = open_db_conection();

        // Insertar en zombies
        $query = "CALL register_new_infected('".$name."')";
        if ($connection->query($query) !== TRUE) {    
            echo "Error inserting record: " . $connection->error;
        }

        close_db_conection($connection);
    }

    function get_zombie_by_id($id) {
        $connection = open_db_conection();
        $sql = "SELECT nombre
                FROM zombies
                WHERE id_zombie = ?";
                
        if (!($statement = $connection->prepare($sql))) 
            die("Error(" . $connection->error . "): " . $connection->error);
        if (!($statement->bind_param("i", $id))) 
            die("Error de vinculación(" . $statement->error . "): " . $statement->error);
        if (!$statement->execute()) 
            die("Error en ejecución de la consulta(" . $statement->error . "): " . $statement->error);

        $result = $statement->get_result();
        close_db_conection($connection);
        return $result;
    }

    // Gets the name and evolution date of all zombies whose latest 
    // evolution is that with id equal to $id_state
    function get_zombies_by_state($id_state) {
        $connection = open_db_conection();
        $sql = "SELECT latest.nombre as nombre, latest.fecha as fecha
                FROM zombie_estado,

                    (SELECT zombies.nombre as nombre, zombies.id_zombie as id_zombie, MAX(zombie_estado.fecha) as fecha
                    FROM zombies, zombie_estado
                    WHERE zombies.id_zombie = zombie_estado.id_zombie
                    GROUP BY zombie_estado.id_zombie, zombies.nombre) as latest
        
                WHERE zombie_estado.id_zombie = latest.id_zombie
                AND zombie_estado.fecha = latest.fecha
                AND zombie_estado.id_estado = ".$id_state;
                
        $result = $connection -> query($sql);

        close_db_conection($connection);
        return $result;
    }

    function get_states() 
    {
        $connection = open_db_conection();
        $sql = "SELECT id_estado, nombre
                FROM estado";
                
        $result = $connection -> query($sql);

        close_db_conection($connection);
        return $result;
    }

    function get_zombies() 
    {
        $connection = open_db_conection();
        $sql = "SELECT id_zombie, nombre
                FROM zombies";
                
        $result = $connection -> query($sql);

        close_db_conection($connection);
        return $result;
    }

    function get_zombie_states($id) {
        $connection = open_db_conection();
        $sql = "SELECT estado.nombre as nombre, zombie_estado.fecha as fecha
                FROM zombies, zombie_estado, estado
                WHERE zombies.id_zombie = zombie_estado.id_zombie 
                AND zombie_estado.id_estado = estado.id_estado
                AND zombie_estado.id_zombie = ?";
                
        if (!($statement = $connection->prepare($sql))) 
            die("Error(" . $connection->error . "): " . $connection->error);
        if (!($statement->bind_param("i", $id))) 
            die("Error de vinculación(" . $statement->error . "): " . $statement->error);
        if (!$statement->execute()) 
            die("Error en ejecución de la consulta(" . $statement->error . "): " . $statement->error);

        $result = $statement->get_result();
        close_db_conection($connection);
        return $result;
    }

    function get_evolutions() {
        $connection = open_db_conection();
        $sql = "SELECT zombies.nombre as znombre, estado.nombre as enombre, zombie_estado.fecha as fecha
                FROM zombies, zombie_estado, estado
                WHERE zombies.id_zombie = zombie_estado.id_zombie 
                AND zombie_estado.id_estado = estado.id_estado
                ORDER BY zombie_estado.fecha DESC";
                
        $result = $connection -> query($sql);

        close_db_conection($connection);
        return $result;
    }
?>