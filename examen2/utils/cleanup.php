<?php
    // Satinizes form input data
    // Parameters:
    //  $data - a value inputted on a form by the user
    // Returns:
    //  The sanitized data
    function cleanup_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
?>