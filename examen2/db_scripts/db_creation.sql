CREATE TABLE `zombies` ( 
    `id_zombie` INT NOT NULL AUTO_INCREMENT , 
    `nombre` VARCHAR(50) NOT NULL , 
    PRIMARY KEY (`id_zombie`)) ENGINE = InnoDB;


CREATE TABLE `estado` ( 
    `id_estado` INT NOT NULL AUTO_INCREMENT , 
    `nombre` VARCHAR(20) NOT NULL , 
    PRIMARY KEY (`id_estado`)) ENGINE = InnoDB;

CREATE TABLE `zombie_estado` ( 
    `id_zombie` INT NOT NULL , 
    `id_estado` INT NOT NULL , 
    `fecha` DATETIME NOT NULL , 
    PRIMARY KEY (`id_zombie`, `id_estado`, `fecha`),
    FOREIGN KEY (`id_zombie`) REFERENCES `zombies` (`id_zombie`),
    FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id_estado`)) ENGINE = InnoDB;




INSERT INTO estado (nombre) VALUES ('infección');
INSERT INTO estado (nombre) VALUES ('desorientación ');
INSERT INTO estado (nombre) VALUES ('violencia');
INSERT INTO estado (nombre) VALUES ('desmayo');
INSERT INTO estado (nombre) VALUES ('transformación');