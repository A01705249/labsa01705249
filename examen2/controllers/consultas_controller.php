<?php
    require_once("../models/model.php");
    require_once("../utils/db_utils.php");

    echo get_zombies_by_state_table($_POST["estado"], $_POST["nombre"]);

    function get_zombies_by_state_table($id_state, $name) {
        $tabla_html = "<table class='table container shadow table-striped table-bordered'>";
        $nombres_encabezados = array("Zombies en estado de ".$name);

        // Encabezado de la tabla
        $tabla_html .= "<thead>";
        $tabla_html .= "<tr>";
        for($i = 0; $i < count($nombres_encabezados); $i++) {
            $tabla_html .= "<th class='text-center' colspan='2'>".$nombres_encabezados[$i]."</th>";
        }
        $tabla_html .= "</tr>";
        $tabla_html .= "</thead>";
        
        // Cuerpo de la tabla
        $tabla_html .= "<tbody>";

        $zombie_rows = get_zombies_by_state($id_state);
        $total = $zombie_rows->num_rows;
       
        while($zombie_row = mysqli_fetch_array($zombie_rows, MYSQLI_ASSOC)) {
            $nombre = $zombie_row["nombre"];
            $fecha = $zombie_row["fecha"];

            $tabla_html .= '<tr>';
            $tabla_html .= "<td colspan='2'>".$nombre." desde ".$fecha."</td>";
            $tabla_html .= '</tr>';
        }    

        $tabla_html .= '<tr>';
        $tabla_html .= "<td>Total</td>";
        $tabla_html .= "<td>".$total."</td>";
        $tabla_html .= '</tr>';

        $tabla_html .= "</tbody>";
        $tabla_html .= "</table>";
        
        mysqli_free_result($zombie_rows); 

        return $tabla_html;
    }
?>