<?php
    session_start();
    
    require_once("../models/model.php");
    require_once("../utils/db_utils.php");
    require_once("../utils/cleanup.php");

    $id_estado = cleanup_input($_POST["id_estado"]);
    $id_zombie = cleanup_input($_POST["id_zombie"]);

    register_new_evolution($id_zombie, $id_estado);

    $_SESSION["mensaje"] = "Se registro el nuevo estado con exito";

    header("Location: ../index.php");
?>