initialize();

function initialize() {
    $("#submit_new").click(ajax_register_new);
    $("#estado").change(ajax_query_by_state);
}

function ajax_register_new() {
    let name = $("#name").val();
    
    $.post(
        'controllers/registrar_nuevo_controller.php', // controller URL
        {"name": name} // data to be sent with the request
    )
    .done(function(data, statusText) {
        $("#register_new_message").html(data);
        $("#name").val("");
    });
}

function ajax_query_by_state() {
    let estado = $("#estado").val();
    let nombre = $("#estado option:selected").text();

    $.post(
        'controllers/consultas_controller.php', // controller URL
        {"estado": estado,
         "nombre": nombre} // data to be sent with the request
    )
    .done(function(data, statusText) {
        $("#by_state_table").html(data);
        $("#name").val("");
    });
}

