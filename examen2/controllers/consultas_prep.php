<?php

    function get_timeline_table() {
        $tabla_html = "<table class='table container shadow table-striped table-bordered'>";
        $nombres_encabezados = array("Zombie", "Nuevo estado", "Fecha y hora del cambio");

        // Encabezado de la tabla
        $tabla_html .= "<thead>";
        $tabla_html .= "<tr>";
        for($i = 0; $i < count($nombres_encabezados); $i++) {
            $tabla_html .= "<th class='text-center'>".$nombres_encabezados[$i]."</th>";
        }
        $tabla_html .= "</tr>";
        $tabla_html .= "</thead>";
        
        // Cuerpo de la tabla
        $tabla_html .= "<tbody>";

        $evol_rows = get_evolutions();

        while($evol_row = mysqli_fetch_array($evol_rows, MYSQLI_ASSOC)) {
            
            $nombre = $evol_row["znombre"];
            $estado = $evol_row["enombre"];
            $fecha =  $evol_row["fecha"];
            
            $tabla_html .= '<tr>';
            $tabla_html .= "<td>".$nombre."</td>";
            $tabla_html .= "<td>".$estado."</td>";
            $tabla_html .= "<td>".$fecha."</td>";
            $tabla_html .= '</tr>';
        }
        
    

        $tabla_html .= "</tbody>";
        $tabla_html .= "</table>";
        
        mysqli_free_result($evol_rows); 

        return $tabla_html;
    }


    function get_proportions_table() {
        $tabla_html = "<table class='table container shadow table-striped table-bordered'>";
        $nombres_encabezados = array("Estado", "Numero de zombies");

        // Encabezado de la tabla
        $tabla_html .= "<thead>";
        $tabla_html .= "<tr>";
        for($i = 0; $i < count($nombres_encabezados); $i++) {
            $tabla_html .= "<th class='text-center'>".$nombres_encabezados[$i]."</th>";
        }
        $tabla_html .= "</tr>";
        $tabla_html .= "</thead>";
        
        // Cuerpo de la tabla
        $tabla_html .= "<tbody>";

        $state_rows = get_states();

        $total = 0;
        $i = 0;
        while($state_row = mysqli_fetch_array($state_rows, MYSQLI_ASSOC)) {
            
            $id_estate = $state_row["id_estado"];
            $nombre = $state_row["nombre"];
            $cantidad = get_zombies_by_state($id_estate)->num_rows;

            $tabla_html .= '<tr>';
            $tabla_html .= "<td>".$nombre."</td>";
            $tabla_html .= "<td>".$cantidad."</td>";
            $tabla_html .= '</tr>';

            $total += $cantidad;
            $i++;
        }    

        $tabla_html .= '<tr>';
        $tabla_html .= "<td>Total</td>";
        $tabla_html .= "<td>".$total."</td>";
        $tabla_html .= '</tr>';

        $tabla_html .= "</tbody>";
        $tabla_html .= "</table>";
        
        mysqli_free_result($state_rows); 

        return $tabla_html;
    }

    $estados = get_states();
?>