<?php
    require_once("../models/model.php");
    require_once("../utils/db_utils.php");
    require_once("../utils/cleanup.php");

    $name = cleanup_input($_POST["name"]);

    register_new_zombie($name);

    echo success_alert($name);

    function success_alert($name) {
        $html = "";
        $html .="<div class='alert alert-success' role='alert'>";
        $html .="<strong>¡Se ha registrado al zombie ".$name."!</strong>. Puedes registrar a otro zombie si es necesario.";
        $html .="</div>";
        return $html;
    }
?>