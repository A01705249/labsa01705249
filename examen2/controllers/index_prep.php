<?php
    
    function get_zombies_table() {
        $tabla_html = "<table class='table container shadow table-striped table-bordered'>";
        $nombres_encabezados = array("Zombie", "Estados", "Acciones");

        // Encabezado de la tabla
        $tabla_html .= "<thead>";
        $tabla_html .= "<tr>";
        for($i = 0; $i < count($nombres_encabezados); $i++) {
            $tabla_html .= "<th class='text-center'>".$nombres_encabezados[$i]."</th>";
        }
        $tabla_html .= "</tr>";
        $tabla_html .= "</thead>";
        
        // Cuerpo de la tabla
        $tabla_html .= "<tbody>";

        $zombie_rows = get_zombies();

        $tabla_html .= '<tr>';

        while($zombie_row = mysqli_fetch_array($zombie_rows, MYSQLI_ASSOC)) {
            
            $id = $zombie_row["id_zombie"];
            $nombre = $zombie_row["nombre"];
            
            $tabla_html .= "<td>".$nombre."</td>";
            $tabla_html .= "<td>";
            $tabla_html .= "<ul>";

            $state_rows = get_zombie_states($id);
            while($state_row = mysqli_fetch_array($state_rows, MYSQLI_ASSOC)) {
                $state = $state_row["nombre"];
                $date = $state_row["fecha"];
                
                
                $tabla_html .= "<li>";
                $tabla_html .= "Paso a ".$state." en ".$date;
                $tabla_html .= "</li>";
                


                
            }
            $tabla_html .= "</ul>";
            $tabla_html .= "</td>";
            $path_actualizar = "registro_actualizacion.php?id=".$id;
            $tabla_html .= '<td class="text-center"><a href="'.$path_actualizar.'" class="btn btn-primary">Agregar estado</a></td> ';

            $tabla_html .= '</tr>';

            mysqli_free_result($state_rows);
        }
        
    

        $tabla_html .= "</tbody>";
        $tabla_html .= "</table>";
        
        mysqli_free_result($zombie_rows); 

        return $tabla_html;
    }



?>
