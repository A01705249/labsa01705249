<?php
    require_once("models/model.php");
    require_once("utils/db_utils.php");
    require_once("controllers/consultas_prep.php");

    include("statics/_header.html");
    include("statics/_navbar.html");

    include("statics/_consultas_body.html");

    include("statics/_footer.html");
?>