<?php
    // Redirect to the login page
    header("Location: login.php");

    // This effect can also be achieved by changing settings on the server
?>