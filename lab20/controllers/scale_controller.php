<?php
    require_once("../models/schedule_model.php");
    require_once("../utils/schedule_utils.php");
    require_once("../utils/db_utils.php");
    require_once("../controllers/schedule_logic.php");

    // The text of this controller will contain the group 
    // scale accurate to the latest selection of the user
    echo get_group_scale_html();   
?>