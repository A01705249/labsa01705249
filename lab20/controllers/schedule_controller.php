<?php
    session_start();

    require_once("../models/schedule_model.php");
    require_once("../utils/schedule_utils.php");
    require_once("../utils/db_utils.php");
    require_once("../controllers/schedule_logic.php");

    // Updates the database to reflect the user's selection
    $selected_cells = $_POST["selected_cells"];
    $username = $_SESSION["username"];
    update_user_schedule($selected_cells, $username);

    // The text of this controller will contain the group 
    // schedule including the changes just made by the user
    echo get_group_schedule_html();   
?>