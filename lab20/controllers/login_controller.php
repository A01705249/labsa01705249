<?php
    session_start();

    require_once("../models/users_model.php");
    require_once("../utils/cleanup.php");
    require_once("../utils/db_utils.php");
   
    // If the form has been submitted
    if (isset($_POST["submit"])) {
        if(isset($_POST["username"]) && isset($_POST["password"])) {

            // Sanitize input data
            $username = cleanup_input($_POST["username"]);
            $password = cleanup_input($_POST["password"]);

            if(user_exists($username)) {
                if(valid_credentials($username, $password)) {
                    $_SESSION["username"] = $username;
                    header("Location: ../schedule.php");
                }
                else {
                    $_SESSION["password_error"] = "La contraseña es incorrecta";
                    $_SESSION["username_value"] = $_POST["username"];
                    header("Location: ../login.php");
                }
            }   
            else {
                if($username !== "") {
                    register_user($username, $password);
                    $_SESSION["username"] = $username;
                    header("Location: ../schedule.php");
                } 
                else {
                    $_SESSION["username_error"] = "Por favor ingresa un nombre de usuario";
                    header("Location: ../login.php");
                }
            }
        } 
        else {
            $_SESSION["form_error"] = "El formulario esta incompleto";
            header("Location: ../login.php");
        }
    }
    else {
        header("Location: ../login.php");
    }

?>