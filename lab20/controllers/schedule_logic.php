<?php
    // Generates the HTML for a **user** schedule. This function is a wrapper for the generic
    // table generation function. It's purpose is to allow data retrieval logic
    // to NOT be located inside the code of the html partial template
    //
    // Parameters:
    //  None
    // Returns:
    //  A string with the generated user schedule HTML
    function get_user_schedule_html() {
        $selected_cells = get_schedule_matrix($_SESSION["username"]);
        return get_schedule_table_html(true, $selected_cells);
    }

    // Generates the HTML for a **group** schedule. This function is a wrapper for the generic
    // table generation function. It's purpose is to allow data retrieval logic
    // to NOT be located inside the code of the html partial template
    //
    // Parameters:
    //  None
    // Returns:
    //  A string with the generated group schedule HTML
    function get_group_schedule_html() {
        $selected_cells = get_schedule_matrix(null);
        return get_schedule_table_html(false, $selected_cells);
    }

    // Generates the HTML for a group scale. This function is a wrapper for the generic
    // scale generation function. It's purpose is to move model and utilities logic
    // away from the files that contain mostly interface code
    //
    // Parameters:
    //  None
    // Returns:
    //  A string with the generated group schedule HTML
    function get_group_scale_html() {
        $selected_cells = get_schedule_matrix(null);
        $scale = get_max($selected_cells);
        return get_scale_html($scale);
    }
?>