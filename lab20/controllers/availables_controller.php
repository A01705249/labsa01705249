<?php
    require_once("../models/schedule_model.php");
    require_once("../utils/schedule_utils.php");
    require_once("../utils/db_utils.php");
    require_once("../controllers/schedule_logic.php");

    $row = $_GET["row"];
    $column = $_GET["column"];

    $available_amount = $_GET["available_amount"]; // The number of users available on a given time
    $total_amount = get_total_users(); // The number of users that have selected their schedule

    $day = get_day($column);
    $hour = get_hour($row);

    $available_users = get_available_users($row, $column);
    $unavailable_users = get_unavailable_users($row, $column);

    echo get_availables_html($available_amount, $total_amount, $day, $hour, $available_users, $unavailable_users);
?>