<?php
    // Generates the HTML for a schedule. The schedule is a table that uses colors to 
    // indicate the availability of an user or group of users at certain times and days.
    // It's meant to generate 2 kinds of schedules depending on the parameter:
    // a schedule for the user to interact with and display the previous selection.
    // and a schedule for all users that cannot be interacted with and merely indicates
    // with color how many users are available at the given times.  
    //
    // Parameters:
    //  $editable       - A boolean value indicating if the schedule will have
    //                    cells that can be changed by the user. Should be true
    //                    when the funtion is to generate the user schedule  
    //  $selected_cells - A 2D array where every position corresponds to a table 
    //                    cell and indicates how many users are available on the
    //                    time denoted by that cell. 
    // Returns:
    //  A string with the generated schedule HTML
    function get_schedule_table_html($editable, $selected_cells){
        $table_html = "";
        $table_html .= "<table class='schedule-table'>";
        
        // Generate the table header with abbreviations for all days of the week
        // They may not be changed due to the way schedules are recorded in the database
        $days = array("Lun", "Mar", "Mier", "Jue", "Vie", "Sab", "Dom");
        $table_html .= "<tr>";
        $table_html .= "<th></th>";
        for($i = 0; $i < count($days); $i++) {
            $table_html .= "<th class='text-center'>".$days[$i]."</th>";
        }
        $table_html .= "</tr>";

        // These variables determine the size and row headers
        // of the schedule. They may not be changed 
        // due to the way schedules are recorded in the database
        $first_hour = 7;
        $last_hour = 22;

        // There are 2 rows for every hour, giving a resolution of 30 min. intervals to the scheduke
        $total_rows = ($last_hour - $first_hour + 1) * 2;
        $total_columns = count($days);

        // This variable is only used when generating for a group of users
        // to calculate the color each cell will have
        $max_users = get_max($selected_cells);

        for($row = 1; $row <= $total_rows; $row++) {
            // The row headers show the corresponding time with AM and PM subfixes  
            $table_html .= "<tr>";
            if(($row % 2) == 1) {
                $time = get_hour($row);
                $table_html .= "<td class='small text-right unselectable time-header-schedule-cell' rowspan='2'><span class='mr-2'>".$time."</span></td>";
            }

            for($column = 1; $column <= $total_columns; $column++) {

                // Determine which styling class this cell should use
                // depending on which half of an hour it represents
                $half_hour_class = "";
                if(($row % 2) == 1) 
                    $half_hour_class = "first-half-schedule-cell";
                else
                    $half_hour_class = "second-half-schedule-cell";
                
                $cell_id = "cell-".$row."-".$column; // This id will through javscript to get a cell given its position. 
                $cell_class = "schedule-cell ".$half_hour_class; 
                $cell_data = "data-row=".$row." data-column=".$column; // This data will be used in javscript to get the position of a cell element
                $cell_style = ""; // Used to asign a calculated color to the cell
                
                if($editable) {
                    $cell_id .= "-editable"; // For interactive schedules the cells get a different id to differentiate them from group schedule cells
                    if($selected_cells[$row][$column] == 1) {
                        $cell_class .= " selected-cell";
                    }
                    else {
                        $cell_class .= " unselected-cell ";
                    }
                }   
                else {
                    // For group schedules each cell gets a color on linear scale, with the color being more intense the more people available
                    $cell_style = "background-color: ".background_color($selected_cells[$row][$column], $max_users);

                    // This data will be used in javascript to get the number of users that selected a cell element
                    $cell_data .= " data-availableamount=".$selected_cells[$row][$column];
                }
                
                $table_html .= "<td id='".$cell_id."' class='".$cell_class."' ".$cell_data." style='".$cell_style."'></td>";
            }
            $table_html .= "</tr>";
        }
        $table_html .= "</table>";

        return $table_html;
    }


    // Generates the HTML for a scale. A scale is a small table that indicates to the user
    // the meaning of each color in the group schedule
    // Parameters:
    //  $scale - an integer represeting the maximum number of users that selected all the same cell
    // Returns:
    //  A string with the generated scale HTML
    function get_scale_html($scale) {
        $table_html = "";
        $table_html .= "<table class='d-flex justify-content-center group-scale-table'>";
        $table_html .= "<tr>";
        $table_html .= "<td><span class='small'>0/".$scale." disponibles</span></td>";

        for($i = 0; $i <= $scale; $i++) {
            $cell_style = "background-color: ".background_color($i, $scale);
            $cell_class = "group-scale-cell";
            $table_cell = "<td class='".$cell_class."' style='".$cell_style."'></td>";
            $table_html .= $table_cell;
        }
        
        $table_html .= "<td><span class='small'>".$scale."/".$scale." disponibles</span></td>";
        $table_html .= "<tr>";
        $table_html .= "</table>";

        return $table_html;
    }

    // Generates the HTML for a section about the available  
    // and unavailable users corresponding to a given time
    // Parameters:
    //  $available_amount  - an integer, how many users are available
    //  $total_amount      - an integer, how many users have selected at least one cell
    //  $day               - a string, the day of the given time
    //  $hour              - a string, the hour of the given time
    //  $available_users   - an array of strings, the usernames of those available at the given time
    //  $unavailable_users - an array of strings, the usernames of those unavailable at the given time
    // Returns:
    //  A string with the generated HTML
    function get_availables_html($available_amount, $total_amount, $day, $hour, $available_users, $unavailable_users) {
        $html = "";
        $html .= "<h4 class='mb-3'>".$available_amount."/".$total_amount." disponibles</h4>";

        $html .= "<p class='small'>Disponibilidad del ".$day." a la ".$hour."</p>";

        $html .= "<table class='d-flex justify-content-center'>";
        $html .= "  <tr>";
        $html .= "      <th class='px-3'>Disponibles</th>";
        $html .= "      <th class='px-3'>No disponibles</th>";
        $html .= "  </tr>";
        

        $rows = max(count($available_users), count($unavailable_users));
        for($i = 0 ; $i < $rows; $i++) {
            $available_user = "";
            $unavailable_user = "";

            if($i < count($available_users)) 
                $available_user = $available_users[$i];
            
            if($i < count($unavailable_users)) 
                $unavailable_user = $unavailable_users[$i];

            $html .= "  <tr>";
            $html .= "      <td>".$available_user."</td>";
            $html .= "      <td>".$unavailable_user."</td>";
            $html .= "  </tr>";
        }
        
        $html .= "</table>";
        return $html;
    }

    // Returns a string with the hour for a row
    // Parameters:
    //  $row - an integer representing the index of a row
    // Returns:
    //  A string representing the corresponging hour
    function get_hour($row) {
        
        $first_hour = 7;

        $hour = $first_hour + floor(($row - 1) / 2);
        $minutes = "";
        $meridian = "";

        if($hour > 12) {
            $hour -= 12; 
            $meridian .= " PM";
        }
        else if($hour == 12){
            $meridian .= " PM";
        }
        else {
            $meridian .= " AM";
        }
       
        if(($row % 2) == 0) {
            $minutes.=":30";
        }

        return ($hour.$minutes.$meridian);
    }

    // Returns a string with the day for a row
    // Parameters:
    //  $column - an integer representing the index of a column
    // Returns:
    //  A string representing the corresponging day
    function get_day($column) {
        $days = array("Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo");
        return $days[$column - 1];
    }

    // Returns the color for a cell in a group schedule.
    // Group schedules have cells with color that vary on the number
    // of users that have selected it, going from white for 0 users
    // to a certain color for the maxium of users
    // Parameters:
    //  $step  - an integer representing how many users selected some cell
    //  $scale - an integer represeting the maximum number of users that selected all the same cell
    // Returns:
    //  The color for a cell given the parameters
    function background_color($step, $scale) {
        // In case the max number of users is 0, just return the value of pure white
        if($scale == 0) {
            return "rgb(255, 255, 255)";
        }

        // This is the color for a cell with the maximum 
        // number of users, it's actually the color of a selected cell
        //$max_color = array(87, 187, 138);
        $max_color = array(51, 153, 0);
        //background-color: rgb(82, 224, 82);
        $rgb = array();
        
        for($i = 0; $i < 3; $i++) {
            $decrement = (255 - $max_color[$i]) / $scale;
            $rgb[$i] = 255 - floor($decrement * $step);
        }

        $color = "rgb(".$rgb[0].", ".$rgb[1].", ".$rgb[2].")";
        return $color;
    }

    // Returns the maximum value of a selection matrix
    // Parameters:
    //  selected_cells - A 2D array where every position corresponds to a table 
    //                    cell and indicates how many users are available on the
    //                    time denoted by that cell.
    // Returns:
    //  The maximum value in the matrix, representing the biggest number
    //  of users that are available at a same time
    function get_max($selected_cells) {
        $max_users = 0;

        // Couldn't find a one liner for this after a few minutes.
        // For a small exercise such as this one, the function is warranted  
        for($i = 1; $i <= 32; $i++) {
            for($j = 1; $j <= 7; $j++) {
                $max_users = max($max_users, $selected_cells[$i][$j]);
            }
        }

        return $max_users; 
    }
?>