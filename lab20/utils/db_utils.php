<?php
    // Opens a conection to a database
    // Parameters:
    //  None, they are hardcoded for the exercise
    // Returns:
    //  A conection object
    function open_db_conection() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "cuandojuntarse";
        
        // Create conection
        $conection = mysqli_connect($servername, $username, $password, $dbname);

        // Check connection
        if (!$conection) {
            die("Connection failed: " . mysqli_connect_error());
        }

        return $conection;
    }

    // Closes a previosly opened database conection
    // Parameters:
    //  $conection - The previously opened database conection object
    // Returns:
    //  Nothing
    function close_db_conection($conection) {
        mysqli_close($conection);
    }

?>