<?php
    // Deletes all selected cells records associated with the user
    // and insert the newly selected cells in the database
    // Parameters:
    //  selected_cells - A 2D array where every position corresponds to a table 
    //                   cell and indicates with a 1 if the user selected it, 0 if not.
    // Returns:
    //  Nothing
    function update_user_schedule($selected_cells, $username) {
        $connection = open_db_conection();

        // Delete all cell records associated with this user
        $query = "DELETE 
                  FROM setcells 
                  WHERE username = '".$username."'";

        if ($connection->query($query) !== TRUE) {    
            echo "Error deleting record: " . $connection->error;
        }

        // Insert new cell records
        for($i = 1; $i <= 32; $i++) {
            for($j = 1; $j <= 7; $j++) {
                if($selected_cells[$i][$j] == 1) {
                    // Notice the use of backtick (`) to get away with having field called "row" and "column"
                    $query = "INSERT INTO setcells (username, `row`, `column`) 
                              VALUES ('".$username."',".$i.",".$j.")"; 
                    if ($connection->query($query) !== TRUE) {    
                        echo "Error inserting record: " . $connection->error;
                    }
                }
            }
        }

        close_db_conection($connection);
    }

    // Returns a 2D array that represents what cells of a schedule
    // are selected. It's intended for both user schedules and group schedules
    // Parameters:
    //  username - the username in the case of requesting the selection matrix of an user
    //             null when requesting the selection matrix for the entire user group 
    // Returns:
    //  selected_cells - A 2D array where every position corresponds to a table 
    //                    cell and indicates how many users are available on the
    //                    time denoted by that cell.
    function get_schedule_matrix($username) {
        $connection = open_db_conection();

        // Initialize empty matrix
        $schedule_matrix = array(array());
        for($i = 1; $i <= 32; $i++) {
            for($j = 1; $j <= 7; $j++) {
                $query = "SELECT *
                          FROM setcells
                          WHERE `row` = ".$i." AND `column` = ".$j;

                if($username !== null) {
                    $query .= " AND username = '".$username."'";
                }
                
                $query_result = $connection->query($query);
                
                // The number of rows returned by the query represents the number
                // of user who selected this cell
                $schedule_matrix[$i][$j] = $query_result->num_rows;
            }
        }

        close_db_conection($connection);

        return $schedule_matrix;
    }

    // Get the number of users that have selected at least one cell
    // Parameters:
    //  None
    // Returns:
    //  An integer representing the number of user that have selected at least one cell
    function get_total_users() {
        $connection = open_db_conection();

        $query = "SELECT DISTINCT users.username 
                  FROM users, setcells
                  WHERE users.username = setcells.username";
        
        $query_result = $connection->query($query);
        
        close_db_conection($connection);

        return $query_result->num_rows;
    }

    // Returns an array of strings with the usernames of all users
    // that have selected a given cell
    //  row    - an integer, the row of the given cell
    //  column - an integer, the column of the given cell
    // Returns:
    //  An array with the usernames of user who have selected the given cell
    function get_available_users($row, $column) {
        $connection = open_db_conection();

        $query = "SELECT users.username AS username
                  FROM users, setcells
                  WHERE users.username = setcells.username
                  AND setcells.row = ".$row." AND setcells.column = ".$column;
        
        $query_result = $connection->query($query);
        
        close_db_conection($connection);
        
        $users = [];
        while ($row = mysqli_fetch_array($query_result, MYSQLI_ASSOC))  {
            array_push($users,$row["username"]);
        }

        mysqli_free_result($query_result); 

        return $users;
    }

    // Returns an array of strings with the usernames of all users
    // that have NOT selected a given cell AND have selected at least one cell
    //  row    - an integer, the row of the given cell
    //  column - an integer, the column of the given cell
    // Returns:
    //  An array of strings with the usernames described above
    function get_unavailable_users($row, $column) {
        $connection = open_db_conection();

        $query = "SELECT username 
                  FROM users
                  WHERE username NOT IN
                    (SELECT users.username AS username
                    FROM users, setcells
                    WHERE users.username = setcells.username
                    AND setcells.row = ".$row." AND setcells.column = ".$column.")
                  AND username IN 
                    (SELECT DISTINCT users.username 
                    FROM users, setcells
                    WHERE users.username = setcells.username)";
        
        $query_result = $connection->query($query);
        
        close_db_conection($connection);
        
        $users = [];
        while ($row = mysqli_fetch_array($query_result, MYSQLI_ASSOC))  {
            array_push($users,$row["username"]);
        }

        mysqli_free_result($query_result); 

        return $users;
    }
?>