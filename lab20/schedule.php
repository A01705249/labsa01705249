<?php
    session_start();

    // If the user doesn't have a session, redirect to login page
    if (!isset($_SESSION["username"])) {
        header("Location: index.php");
    }

    require_once("models/schedule_model.php");
    require_once("utils/schedule_utils.php");
    require_once("utils/db_utils.php");
    require_once("controllers/schedule_logic.php");    

    include("statics/_header.html");
    include("statics/_navbar.html");

    // This is the partial that contains the page content
    include("statics/_schedule_body.php");

    include("statics/_footer.html");
?>