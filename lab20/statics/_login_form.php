<div class="row container-fluid ">
    <div class="col-sm-12 col-md-8 col-lg-5 container bg-white shadow rounded my-5 p-5">
        <form action="controllers/login_controller.php" method="POST">
            
            <h5 class="text-center">Iniciar sesión</h5>
            <p class="text-center">Si nunca haz entrado, inventa un nombre de usuario y una contraseña.</p>

            <!-- Username -->
            <div class="form-group">
                <label for="username">Nombre:</label>
                <input type="text" class="form-control" id="username" name="username" <?php if(isset($_SESSION["username_value"])) {echo "value='".$_SESSION['username_value']."'";unset($_SESSION["username_value"]);}?> required>
                <?php if(isset($_SESSION["username_error"])): ?>
                    <small class="form-text text-danger"><?= $_SESSION["username_error"]; ?></small>
                    <?php unset($_SESSION["username_error"]) ?>
                <?php endif; ?>
            </div>

            <!-- Password -->                
            <div class="form-group">
                <label for="password">Contraseña</label>
                <input type="password" class="form-control" id="password" name="password" required>
                <?php if(isset($_SESSION["password_error"])): ?>
                    <small class="form-text text-danger"><?= $_SESSION["password_error"]; ?></small>
                    <?php unset($_SESSION["password_error"]) ?>
                <?php endif; ?>
            </div>
            
            <?php if(isset($_SESSION["form_error"])): ?>
                <small class="form-text text-danger"><?= $_SESSION["form_error"]; ?></small>
                <?php unset($_SESSION["form_error"]) ?>
            <?php endif; ?>

            <!-- Submit button -->
            <div class="d-flex justify-content-center mt-4">
                <button type="submit" class="btn btn-success" name="submit">Iniciar sesión</button>
            </div>
        </form>
    </div> 
</div> 
