<div class=" container-fluid" ondragstart="return false;" ondrop="return false;">
    <div class="row bg-white shadow rounded my-5 p-5">

        <!-- This is the div for the user related content-->
        <div id="user-container" class="col-lg text-center">
        
            <div>
                <h4 class="mb-3"><?php echo "Disponibilidad de ".$_SESSION["username"]?></h4>

                <!-- Symbology: a description for types of cells -->
                <table class="d-flex justify-content-center">
                    <tr>
                        <td><span class="small">No disponible</span></td><td class="schedule-cell unselected-cell d-inline-block"></td>
                        <td class="pl-3"><span class="small">Disponible</span></td><td class="schedule-cell selected-cell d-inline-block"></td>
                    </tr>
                </table>

                <p class="small">Haz click y arrastra para seleccionar. Los cambios son guardados automáticamente.</p>
            </div>
            
            <!--  User schedule -->
            <div class="d-flex justify-content-center">
                <?=get_user_schedule_html()?>
            </div>
            
        </div>

        <!-- This div is used to display the users available at the time of the group cell hovered by the user -->
        <div id="availables-container" class="col-lg text-center d-none"></div>

        <!-- This is the div for the group related content-->
        <div class="col-lg text-center mt-5 mt-lg-0">
            
            <div>
                <h4 class="mb-3">Disponibilidad del grupo</h4>

                <!-- Symbology: a scale to indicate the meaning of each color -->
                <!--The content of this div changes throug AJAX-->
                <div id="group-scale">
                    <?=get_group_scale_html()?>
                </div>  
                
                <p class="small">Mueve el cursor sobre las celdas para consultar y ver quien esta disponible.</p>
            </div>

            <!-- Group schedule -->
            <!--The content of this div changes throug AJAX-->
            <div id="group-schedule" class="d-flex justify-content-center">
                <?=get_group_schedule_html()?>
            </div>
        </div>
        
    </div>
</div>

