DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `register_user`(
    IN username VARCHAR(50),
    IN password VARCHAR(50))
BEGIN
    INSERT INTO users VALUES (username, password);
END$$
DELIMITER ;