<?php
    session_start();

    // If the user already has a session, redirect to schedule selection
    if (isset($_SESSION["username"])) {
        header("Location: schedule.php");
    }

    include("statics/_header.html");
    include("statics/_navbar.html");

    include("statics/_login_form.php");
    
    include("statics/_footer.html");
?>