-- Las sentencias para agregar llaves primarias
ALTER TABLE Materiales add constraint llaveMateriales PRIMARY KEY (Clave)
ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero)
ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)
ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY (Clave, RFC, Numero, Fecha)

-- Para agregar llaves foraneas
ALTER TABLE entregan add constraint cfentreganclave
foreign key (clave) references materiales(clave);

ALTER TABLE entregan add constraint cfentreganrfc
foreign key (RFC) references proveedores(RFC);

ALTER TABLE entregan add constraint cfentregannumero
foreign key (Numero) references proyectos(Numero);

-- Para que las cantidades en entregan sean mayor a 0
ALTER TABLE entregan add constraint cantidad check (cantidad > 0) 