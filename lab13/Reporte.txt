Ejercicio 2

Despues de insertar los valores (1000, 'xxx', 1000) a la tabla de materiales
tenemos un problema: ya habia un registro con la misma clave en la tabla.
El DBMS lo permite solo porque hasta ahora no se ha indentificado ese 
atributo como clave.
Despues de agregar la restriccion de la llave primaria de materiales el 
DBMS no permite agregar los valores anteriores justamente porque viola 
esta restriccion.

El comando sp_helpconstraint materiales nos muestra, como es de esperar,
informacion sobre las restricciones que hemos agregado a materiales.
En el caso de la ultima restriccion recien impuesta se puede apreciar su
nombre y el campo que es lave primaria.

Para agregar las llaves de las otras tablas usamos los siguientes comandos:
	ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero)
	ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)
	ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY (Clave, RFC, Numero, Fecha)
Podemos notar que en el caso de la tabla Entregan la clave esta compuesta
por varias columnas. Esto se indica como una lista de parametros dentro de 
los parentesis.


Ejercicio 3

Insertar los valores (0, 'xxx', 0, '1-jan-02', 0) nos deja con un registro
consistente en entregan porque los atributos que son llaves foraneas no 
aparecen en sus respectivas tablas. Es un registro que no se enlaza con nada.
El DBMS mientras tanto esta contento de ejecutar la sentencia ya que no le hemos
dicho sobre llaves foraneas.

Despues de agregar restricciones sobre las llaves foraneas en la tabla entregan,
no nos perite ingresar los valores anteriores, rompe estas restricciones. 

Usar 1sp_helpconstraint una vez mas nos da informacion sobre las restricciones que
hemos agregado a alguna tabla. Nos indica para cada restriccion su tipo, nombre, 
columnas que participan, si se encuentra activa y que accion tomar al borrar o 
modificar un registro.


Ejercicio 4

Observando estos valores insertados en entregan:
(1000, 'AAAA800101', 5000, GETDATE(), 0)
Notamos el uso de la funcion GETDATE() para registrar la hora y fecha actuales,
y que el valor del campo cantidad es 0, lo cual no tiene sentido. ¿Por que registrarias
una entrega de nada?

Y de nuevo otra vez, tras agregar una restriccion que obliga a que la cantidad sea mayor
a 0, el registro anterior lanza un error explicandonos que los valores estan
conflictuados con esa restriccion.


La integridad referencial se refiere a que en una base de datos relacional, como las que
hemos venido trabajando, cada vez que se use una llave foranea esta debe referenciar
a una llave primaria de otra tabla. 
Mantener integridad ref. en general significa tomar acciones para asegurar esto en todo momento,
ya sea que se agreguen, modifiquen o borren registros.
Por ejemplo, la tabla entregan tiene 3 llaves foraneas, una por cada tabla a la que esta relacionada.
En este caso significa que para cada registro de entregan:
-la llave foranea que referencia a materiales (clave en entregan) debe tener una valor que exista como
llave primaria (clave en materiales)de algun registro en materiales
-lo mismo analogamente para proveedores y proyectos
En otras palabras, si en los registros de entregan se guarda la clave de algun material para relacionarlos,
esa clave debe ser valida y existir en materiales, de otro modo ¿a que referenciariamos?

Fuentes consultadas:
https://database.guide/what-is-referential-integrity/
https://en.wikipedia.org/wiki/Referential_integrity
https://www.w3resource.com/sql/joins/joining-tables-through-referential-integrity.php








